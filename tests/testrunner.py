# coding=utf-8
""""

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-07'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'


from django.test.runner import DiscoverRunner


class SIIMDiscoverRunner(DiscoverRunner):

    def setup_databases(self, **kwargs):
        # Remove signaler_iim
        kwargs['aliases'].remove('signaler_iim')
        old_names = super(SIIMDiscoverRunner, self).setup_databases(**kwargs)
        return old_names
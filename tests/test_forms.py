# coding=utf-8
"""" Test Signaler IIM forms

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-14'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.conf import settings
from django.test import override_settings, TestCase
from django.test.client import RequestFactory
from django.core.files import File
from qdjango.tests.base import QdjangoTestBase, DATASOURCE_PATH
from usersmanage.tests.utils import setup_testing_user
from core.models import Group as CoreGroup, G3WSpatialRefSys
from qdjango.utils.data import QgisProject
from usersmanage.models import Group as AuthGroup, User, Userbackend, USER_BACKEND_DEFAULT
from usersmanage.configs import G3W_VIEWER1
from signaler_iim.forms import SIIMProjectForm, SIIMUserForm
from .utils import SLS_PROJECT_RIGHT, CURRENT_PATH, TEST_BASE_PATH, SLS_PROJECT_RIGHT


@override_settings(
    CACHES={
        'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'some',
        }
    },
    DATASOURCE_PATH=DATASOURCE_PATH,
    LANGUAGE_CODE='en',
    LANGUAGES=(
        ('en', 'English'),
    ),
)
class SIIMFormsTest(QdjangoTestBase):

    databases = {'default', 'signaler_iim'}

    @classmethod
    def setUpTestData(cls):

        super().setUpTestData()

        cls.request = RequestFactory()


        # main project group
        cls.project_group = CoreGroup(name='GroupIIM', title='GroupIIM', header_logo_img='',
                                      srid=G3WSpatialRefSys.objects.get(auth_srid=3857))

        cls.project_group.save()

        qgis_project_file = File(open('{}{}{}'.format(CURRENT_PATH, TEST_BASE_PATH, SLS_PROJECT_RIGHT), 'r'))
        cls.project = QgisProject(qgis_project_file)
        cls.project.title = 'SIIM project'
        cls.project.group = cls.project_group
        cls.project.save()

        # Create iim_responsible users
        cls.test_iim_responsible = User.objects.create_user(username='test_iim_responsible', password='test_iim_responsible')
        cls.test_iim_responsible.save()
        cls.test_iim_responsible.groups.add(AuthGroup.objects.get(name=G3W_VIEWER1))
        cls.test_iim_responsible.groups.add(AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['iim_responsible']))
        Userbackend(user=cls.test_iim_responsible, backend=USER_BACKEND_DEFAULT).save()

    def test_validation_siimprojectform(self):
        """Testing validation of SIIMProjectForm"""

        self.request.user = self.test_admin1

        # Empty form
        # -------------------------------------
        form = SIIMProjectForm(request=self.request)
        self.assertFalse(form.is_valid())

        # Validate project
        # -------------------------------------
        form_data = {
            'type': '',
            'project': self.project.instance.pk
        }

        form = SIIMProjectForm(request=self.request, data=form_data)
        self.assertTrue(form.is_valid())

    def test_validation_users_form(self):
        """ Test validations cases for SIIM user form """

        # Test as admin01
        self.request.user = self.test_admin1



        # Empty form
        # -------------------------------------
        form = SIIMUserForm(request=self.request)
        self.assertFalse(form.is_valid())

        form_data = {

        }

        form = SIIMUserForm(request=self.request, data=form_data)
        self.assertFalse(form.is_valid())

        self.assertEqual(form.errors['first_name'], ['This field is required.'])
        self.assertEqual(form.errors['last_name'], ['This field is required.'])
        self.assertEqual(form.errors['email'], ['This field is required.'])
        self.assertEqual(form.errors['username'], ['This field is required.'])
        self.assertEqual(form.errors['password1'], ['This field is required.'])
        self.assertEqual(form.errors['password2'], ['This field is required.'])
        self.assertEqual(form.errors['group'], ['This field is required.'])

        # Test CP and IIM_OFFICE field

        form_data = {
            'username': 'test_ccp_reporter',
            'first_name': 'john',
            'last_name': 'red',
            'email': 'j.red@email.com',
            'password1': 'gstehdf63857fhtu8',
            'password2': 'gstehdf63857fhtu8',
            'group': AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['ccp_reporter']).pk
        }

        form = SIIMUserForm(request=self.request, data=form_data)
        self.assertFalse(form.is_valid())

        self.assertEqual(form.errors['cp'], [f'If role selected is one of SIIM-CCP-REPORTERS or SIIM-CCP-SUPERVISORS , '
                                             f'\'Port authorities\' must not be empty.'])

        form_data = {
            'username': 'test_ccp_supervisor',
            'first_name': 'john',
            'last_name': 'red',
            'email': 'j.red@email.com',
            'password1': 'gstehdf63857fhtu8',
            'password2': 'gstehdf63857fhtu8',
            'group': AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['ccp_supervisor']).pk
        }


        form = SIIMUserForm(request=self.request, data=form_data)
        self.assertFalse(form.is_valid())

        self.assertEqual(form.errors['cp'], [f'If role selected is one of SIIM-CCP-REPORTERS or SIIM-CCP-SUPERVISORS , '
                                             f'\'Port authorities\' must not be empty.'])

        form_data = {
            'username': 'test_iim_examiner',
            'first_name': 'john',
            'last_name': 'red',
            'email': 'j.red@email.com',
            'password1': 'gstehdf63857fhtu8',
            'password2': 'gstehdf63857fhtu8',
            'group': AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['iim_examiner']).pk
        }

        form = SIIMUserForm(request=self.request, data=form_data)
        self.assertFalse(form.is_valid())

        self.assertEqual(form.errors['iim_office'], [f'If role selected is one of '
                                                     f'SIIM-IIM-EXAMINERS or SIIM-IIM-RESPONSIBLES , '
                                                     f'\'Office\' must not be empty.'])

        form_data = {
            'username': 'test_iim_examiner',
            'first_name': 'john',
            'last_name': 'red',
            'email': 'j.red@email.com',
            'password1': 'gstehdf63857fhtu8',
            'password2': 'gstehdf63857fhtu8',
            'group': AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['iim_examiner']).pk,
            'cp': '1',
            'iim_office': 'sge'
        }

        form = SIIMUserForm(request=self.request, data=form_data)
        self.assertFalse(form.is_valid())

        self.assertEqual(form.errors['iim_office'], ['\'Port authorities\' or \'Office\' can be set, '
                                                     'not at the same time.'])

        self.assertEqual(form.errors['cp'], ['\'Port authorities\' or \'Office\' can be set, not at the same time.'])


        # Test with siim_responsible user
        self.request.user = self.test_iim_responsible

        form_data = {
            'username': 'test_iim_examiner',
            'first_name': 'john',
            'last_name': 'red',
            'email': 'j.red@email.com',
            'password1': 'gstehdf63857fhtu8',
            'password2': 'gstehdf63857fhtu8',
            'group': AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['iim_responsible']).pk
        }

        form = SIIMUserForm(request=self.request, data=form_data)
        self.assertFalse(form.is_valid())

        self.assertEqual(form.errors['group'], ['Select a valid choice. That choice is not one '
                                                'of the available choices.'])

    def test_users_form(self):
        """ Test CRUD cfor SIIM user form """

        # Test as admin01
        self.request.user = self.test_admin1

        # Test CRUD iim_examiner
        # ------------------------
        group = AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['iim_examiner'])

        form_data = {
            'username': 'test_iim_examiner',
            'first_name': 'john',
            'last_name': 'red examiner',
            'email': 'j.red_examiner@email.com',
            'password1': 'gstehdf63857fhtu8',
            'password2': 'gstehdf63857fhtu8',
            'group': group.pk,
            'iim_office': 'sge'
        }

        form = SIIMUserForm(request=self.request, data=form_data)
        self.assertTrue(form.is_valid())
        form.save()

        user = User.objects.get(username='test_iim_examiner')
        self.assertEqual(user.last_name, 'red examiner')
        self.assertEqual(user.email, 'j.red_examiner@email.com')

        self.assertEqual(user.siimuserextradata.iim_office, 'sge')
        self.assertIsNone(user.siimuserextradata.cp)

        self.assertIn(group, user.groups.all())

        form_data = {
            'username': 'test_iim_examiner',
            'first_name': 'john',
            'last_name': 'red examiner 2',
            'email': 'j.red_examiner@email.com',
            'password1': 'gstehdf63857fhtu8',
            'password2': 'gstehdf63857fhtu8',
            'group': group.pk,
            'iim_office': 'idr'
        }

        form = SIIMUserForm(request=self.request, data=form_data, instance=user)
        self.assertTrue(form.is_valid())
        form.save()

        user = User.objects.get(username='test_iim_examiner')
        self.assertEqual(user.last_name, 'red examiner 2')
        self.assertEqual(user.email, 'j.red_examiner@email.com')

        self.assertEqual(user.siimuserextradata.iim_office, 'idr')
        self.assertIsNone(user.siimuserextradata.cp)

        self.assertIn(group, user.groups.all())

        # Change role to ccp_reporter
        group = AuthGroup.objects.get(name=settings.SIIM_USERS_GROUP['ccp_reporter'])

        form_data = {
            'username': 'test_ccp_reporter',
            'first_name': 'john',
            'last_name': 'red reporter',
            'email': 'j.red_examiner@email.com',
            'password1': 'gstehdf63857fhtu8',
            'password2': 'gstehdf63857fhtu8',
            'group': group.pk,
            'cp': 1
        }

        form = SIIMUserForm(request=self.request, data=form_data, instance=user)
        self.assertTrue(form.is_valid())
        form.save()

        user = User.objects.get(username='test_ccp_reporter')
        self.assertEqual(user.last_name, 'red reporter')
        self.assertEqual(user.email, 'j.red_examiner@email.com')

        self.assertEqual(user.siimuserextradata.cp, 1)
        self.assertTrue(user.siimuserextradata.iim_office == '')

        self.assertIn(group, user.groups.all())







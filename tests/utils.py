# coding=utf-8
""""

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-14'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'



import os

CURRENT_PATH = os.getcwd()
TEST_BASE_PATH = '/signaler_iim/tests/data/'
SLS_PROJECT_RIGHT = 'iim_iim-segnalamneti-luminosi-sonori.qgs'

DATASOURCE_PATH = f'{CURRENT_PATH}{TEST_BASE_PATH}geodata'
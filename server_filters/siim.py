# coding=utf-8
"""" QGIS server filter for module Signaler IIM

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-12'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.conf import settings
from qgis.server import QgsAccessControlFilter
from qgis.core import QgsMessageLog, Qgis
from qdjango.apps import QGS_SERVER
from qdjango.models import SessionTokenFilter, Layer
from usersmanage.utils import userHasGroups
from signaler_iim.models import SIIMSignalerProjects, SlsbCp, SIIM_SIGNAL_TYPES
from signaler_iim.utils import  get_layer_orignames


class SIIMSignalByUserAccessControlFilter(QgsAccessControlFilter):
    """A filter that sets an expression filter for to filter layer features by request user"""

    def __init__(self, server_iface):
        super().__init__(server_iface)

    def layerFilterExpression(self, layer):
        """Retrieve and sets user filter"""

        try:
            qdjango_layer = Layer.objects.get(project=QGS_SERVER.project, qgs_layer_id=layer.id())
        except Layer.DoesNotExist:
            return ""

        signal_type = SIIMSignalerProjects.get_type(QGS_SERVER.project)

        if not signal_type:
            return ""
        layers = get_layer_orignames(signal_type, QGS_SERVER.project, '_')

        # Only for signal data layer by signal type
        if layer.id() not in layers.values():
            return ""

        expr = ""

        # Filter only for user ccp_reporter
        if userHasGroups(QGS_SERVER.user,
                         [settings.SIIM_USERS_GROUP['ccp_reporter'], settings.SIIM_USERS_GROUP['ccp_supervisor']]):

            if hasattr(QGS_SERVER.user, 'siimuserextradata'):

                # if SIIM_SIGNAL_TYPES[signal_type]['signaler_layer_origname'] == layer.name():
                expr = f"\"capit_porto\" = '{QGS_SERVER.user.siimuserextradata.cp}'"
                # else:
                #
                #     # Get signal id with same CP
                #     model = SIIM_SIGNAL_TYPES[signal_type]['model']
                #     signals = "'"+"','".join([str(s.pk) for s in model.objects.filter(capit_porto=QGS_SERVER.user.siimuserextradata.cp)])+"'"
                #     if signals:
                #         expr = f"\"signal_id\" IN ({signals})"

        elif userHasGroups(QGS_SERVER.user,
                               [settings.SIIM_USERS_GROUP['iim_responsible'], settings.SIIM_USERS_GROUP['iim_examiner']]):

            indexes_model = SIIM_SIGNAL_TYPES[signal_type]['indexes_model']
            idxes = indexes_model.objects.filter(s_passaggio__in=['1IL', '2DV'])

            signals = None
            if SIIM_SIGNAL_TYPES[signal_type]['signaler_layer_origname'] == qdjango_layer.origname:
                idxes = idxes.distinct('s_id')
                signals = ",".join([str(i.s_id) for i in idxes])
            elif SIIM_SIGNAL_TYPES[signal_type]['geo_layer_origname'] == qdjango_layer.origname:
                idxes = idxes.distinct('sf_id')
                signals = ",".join([str(i.sf_id) for i in idxes if i.sf_id])
            elif SIIM_SIGNAL_TYPES[signal_type]['vertex_layer_origname'] == qdjango_layer.origname:
                idxes = idxes.distinct('sv_id')
                signals = ",".join([str(i.sv_id) for i in idxes if i.sv_id])

            if signals:
                expr = f"\"id\" NOT IN ({signals})"

        return expr

    def cacheKey(self):
        """Return a cache key, a contant value means that the cache works
        normally and this filter does not influence the cache, an empty value
        (which is the default implementation) means that the cache is disabled"""

        # Return a constant: the cache is not influenced by this filter
        return "siim"


# Register the filter, keep a reference because of the garbage collector
filter = SIIMSignalByUserAccessControlFilter(QGS_SERVER.serverInterface())
QGS_SERVER.serverInterface().registerAccessControl(filter, 9900)
from django.apps import AppConfig, apps
from django.db.models.signals import post_migrate
from core.utils.general import getAuthPermissionContentType


def add_perms2groups(sender, **kwargs):

    from django.conf import settings

    # Create Signaler IIM VIEWER Groups and give permissions
    AuthGroup, Permission, ContentType = getAuthPermissionContentType()
    GroupRole = apps.get_app_config('usersmanage').get_model('GroupRole')
    SIIMSignalerProjects = apps.get_app_config('signaler_iim').get_model('siimsignalerprojects')

    for t, ug in settings.SIIM_USERS_GROUP.items():
        viewers, created = AuthGroup.objects.get_or_create(name=ug)

        try:
            grouprole = viewers.grouprole
            grouprole.role = 'viewer'
        except:
            grouprole = GroupRole(group=viewers, role='viewer')

        grouprole.save()

        viewers_perms = viewers.permissions.all()

        permissions_to_add = (
            Permission.objects.get(codename='manage_reports',
                                   content_type=ContentType.objects.get_for_model(SIIMSignalerProjects)),
        )


        if t in ('ccp_reporter', 'ccp_supervisor'):
            permissions_to_add += (
                Permission.objects.get(codename='make_report',
                                       content_type=ContentType.objects.get_for_model(SIIMSignalerProjects)),
            )

        if t == 'iim_responsible':
            permissions_to_add += (
                Permission.objects.get(codename='create_siim_users',
                                       content_type=ContentType.objects.get_for_model(SIIMSignalerProjects)),
                Permission.objects.get(codename='update_siim_users',
                                       content_type=ContentType.objects.get_for_model(SIIMSignalerProjects)),
                Permission.objects.get(codename='view_siim_users',
                                       content_type=ContentType.objects.get_for_model(SIIMSignalerProjects)),
            )

        for perm in permissions_to_add:
            if perm not in viewers_perms:
                viewers.permissions.add(perm)


class SignalerIimConfig(AppConfig):
    name = 'signaler_iim'

    def ready(self):

        # import signal handlers
        import signaler_iim.receivers

        from django.conf import settings
        from signaler_iim import settings as siim_settings

        # Check for editing_imm project, if is not set raise exception
        if 'editing_iim' not in settings.INSTALLED_APPS:
            raise Exception('\'singaler_iim\' required \'editing_imm\' module')

        for a in dir(siim_settings):
            if not a.startswith('__') and not hasattr(settings, a):
                setattr(settings, a, getattr(siim_settings, a))

        post_migrate.connect(add_perms2groups, sender=self)

        # Load all QGIS server filter plugins, apps can load additional filters
        # by registering them directly to QGS_SERVER
        # Load all QGIS server services plugins, apps can load additional services
        # by registering them directly to QGS_SERVER
        from . import server_filters




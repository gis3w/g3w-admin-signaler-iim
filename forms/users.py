# coding=utf-8
"""" User form for Singaler IIM module

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-02'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.conf import settings
from django.contrib.auth.forms import \
    UserCreationForm, \
    ReadOnlyPasswordHashField
from django.contrib.auth import password_validation
from django.forms import \
    CharField, \
    ModelChoiceField, \
    PasswordInput, \
    ValidationError, \
    ChoiceField
from django.db import transaction
from django.utils.translation import ugettext, ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, HTML, Field
from crispy_forms.bootstrap import PrependedText

from core.mixins.forms import G3WRequestFormMixin, G3WFormMixin
from usersmanage.models import Group as AuthGroup, Userbackend, USER_BACKEND_DEFAULT
from usersmanage.configs import G3W_VIEWER1
from usersmanage.utils import userHasGroups
from signaler_iim.models import SlsbCp, SIIMUserExtraData, IIM_DEPARTMENTS

import copy


class SIIMUserForm(G3WRequestFormMixin, G3WFormMixin, UserCreationForm):

    # For ACL
    group = ModelChoiceField(
        queryset=None,
        required=True,
        help_text=_('Select the role for this user'),
        label=_('Main role')
    )

    phone = CharField(label=_('Phone number'), max_length=16, required=False)

    # For ccp users
    cp = ModelChoiceField(label=_('Port authorities'), queryset=SlsbCp.objects.all(), required=False)

    # For iim users
    iim_office = ChoiceField(label=_('Office'), choices=IIM_DEPARTMENTS, required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set role based on request user main role
        if self.request.user.is_superuser:
            qs = AuthGroup.objects.filter(name__in=settings.SIIM_USERS_GROUP.values())
        else:
            groups = copy.copy(settings.SIIM_USERS_GROUP)
            del(groups['iim_responsible'])
            qs = AuthGroup.objects.filter(name__in=groups.values())

        self.fields['group'].queryset = qs


        # Set required fields
        for f in ('first_name', 'last_name', 'email'):
            self.fields[f].required = True

        self.helper = FormHelper(self)
        self.helper.form_tag = False

        self.helper.layout = Layout(Div(
                Div(
                    Div(
                        Div(
                            HTML(
                                "<h3 class='box-title'><i class='fa fa-file'></i> {}</h3>".format(_('Anagraphic'))),
                            css_class='box-header with-border'
                        ),
                        Div(
                            'first_name',
                            'last_name',
                            'email',
                            'phone',
                            Field('cp', css_class='select2'),
                            Field('iim_office', css_class='select2'),
                            css_class='box-body',

                        ),
                        css_class='box box-success'
                    ),
                    css_class='col-md-6'
                ),

                Div(
                    Div(
                        Div(
                            Div(
                                HTML(
                                    "<h3 class='box-title'><i class='fa fa-lock'></i> {}</h3>".format(_('Login data'))),
                                css_class='box-header with-border'
                            ),
                            Div(
                                'group',
                                PrependedText('username', '<i class="fa fa-user"></i>'),
                                PrependedText('password1', '<i class="fa fa-lock"></i>'),
                                PrependedText('password2', '<i class="fa fa-lock"></i>'),
                                css_class='box-body',

                            ),
                            css_class='box box-danger'
                        ),
                        css_class='col-md-6'
                    ),
                    css_class='row'
                ),
            ),
        )

    def clean_cp(self):

        # check Group
        if 'group' in self.cleaned_data:

            roles = [settings.SIIM_USERS_GROUP['ccp_reporter'], settings.SIIM_USERS_GROUP['ccp_supervisor']]

            if self.cleaned_data['group'].name in roles:

                # Check if is no empty
                if not self.cleaned_data['cp']:
                    raise ValidationError(_(f'If role selected is one of {" or ".join(roles)} , \'Port authorities\''
                                            f' must not be empty.'))

        return self.cleaned_data['cp']

    def clean_iim_office(self):

        # check Group
        if 'group' in self.cleaned_data:

            if self.request.user.is_superuser:
                roles = [settings.SIIM_USERS_GROUP['iim_examiner'], settings.SIIM_USERS_GROUP['iim_responsible']]
            else:
                roles = [settings.SIIM_USERS_GROUP['iim_examiner']]

            if self.cleaned_data['group'].name in roles:

                # Check if is no empty
                if not self.cleaned_data['iim_office']:
                    raise ValidationError(_(f'If role selected is one of {" or ".join(roles)} , \'Office\''
                                            f' must not be empty.'))

        return self.cleaned_data['iim_office']

    def clean(self):
        cleaned_data = super().clean()

        # Check for office and cp at th same time
        if 'iim_office' in self.cleaned_data and self.cleaned_data['iim_office'] and \
                'cp' in self.cleaned_data and self.cleaned_data['cp']:
            self.add_error('iim_office', _(f'\'Port authorities\' or \'Office\' can be set, not at the same time.'))
            self.add_error('cp', _(f'\'Port authorities\' or \'Office\' can be set, not at the same time.'))

        return cleaned_data

    @transaction.atomic
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)

        if self.cleaned_data['password1']:
            user.set_password(self.cleaned_data['password1'])

        # Save data base user
        user.save()

        # Add viewer 1 role if is not set
        if not userHasGroups(user, [G3W_VIEWER1]):
            AuthGroup.objects.get(name=G3W_VIEWER1).user_set.add(user)

        # Manage main IIM role
        if 'group' in self.cleaned_data:
            for g in settings.SIIM_USERS_GROUP.values():
                ag = AuthGroup.objects.get(name=g)
                if self.cleaned_data['group'] == ag:
                    ag.user_set.add(user)
                else:
                    ag.user_set.remove(user)

        # Save backend: hardcoded to G3WSUITE
        if not hasattr(user, 'userbackend'):
            Userbackend(user=user, backend=USER_BACKEND_DEFAULT).save()

        if hasattr(user, 'siimuserextradata'):

            # Save cp for ccp users
            user.siimuserextradata.cp = self.cleaned_data['cp'].pk if self.cleaned_data['cp'] else None

            # Save iim_office fro iim users
            user.siimuserextradata.iim_office = self.cleaned_data['iim_office']

            # For phone field
            user.siimuserextradata.phone = self.cleaned_data['phone']

            user.siimuserextradata.save()
        else:
            SIIMUserExtraData(user=user,
                              cp=self.cleaned_data['cp'].pk if self.cleaned_data['cp'] else None,
                              iim_office=self.cleaned_data['iim_office'],
                              phone=self.cleaned_data['phone']
                              ).save()

    class Meta(UserCreationForm.Meta):
        fields = (
            'first_name',
            'last_name',
            'email',
            'username',
            'password1',
            'password2'
        )


class SIIMUserUpdateForm(SIIMUserForm):

    password1 = CharField(label=_("Password"), widget=PasswordInput, required=False)
    password2 = CharField(label=_("Password confirmation"), widget=PasswordInput, required=False,
                                help_text=_("Enter the same password as above, for verification."))

    password = ReadOnlyPasswordHashField()

    def clean_password(self):
        return self.initial['password']

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')
        if password1:
            password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2
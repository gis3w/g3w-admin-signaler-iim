# coding=utf-8
"""" Admin forms for Signaler IIM module.

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-04-08'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'


from django.forms.models import ModelForm, ValidationError
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, HTML, Row, Field, Hidden
# from usersmanage.forms import G3WACLForm
from usersmanage.utils import crispyBoxACL, userHasGroups
from usersmanage.configs import G3W_EDITOR1
from core.mixins.forms import G3WRequestFormMixin, G3WFormMixin
from editing.models import G3WEditingLayer
from signaler_iim.models import SIIMSignalerProjects, SIIM_SIGNAL_TYPES
from signaler_iim.utils import get_layer_orignames
import copy


class SIIMProjectForm(G3WFormMixin, G3WRequestFormMixin, ModelForm):
    """
    Form for CRUD of SIIMSignalerProjects model.
    """
    class Meta:
        model = SIIMSignalerProjects
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.layout = Layout(
                                Div(
                                    Div(
                                        Div(
                                            Div(
                                                HTML("<h3 class='box-title'><i class='fa fa-file'></i> {}</h3>".format(
                                                    _('Project'))),
                                                css_class='box-header with-border'
                                            ),
                                            Div(
                                                Field('type', css_class='select2'),
                                                Field('lang', css_class='select2'),
                                                Field('project', css_class='select2'),
                                                Field('note', css_class='wys5'),
                                                css_class='box-body',
                                            ),
                                            css_class='box box-success'
                                        ),
                                        css_class='col-md-12'
                                    ),
                                    css_class='row'
                                ),
                            )

    def clean_type(self):

        fields = ('signaler_layer_origname', 'geo_layer_origname', 'vertex_layer_origname')

        type = SIIM_SIGNAL_TYPES[self.cleaned_data['type']]

        # Check for settings: signaler_layer_origname
        if not type[fields[0]]:
            raise ValidationError(
                _(f"The config \"{fields[0]}\" parameter for type \"{type['desc']}\" is not set!"))

        if self.cleaned_data['type'] not in ('pla', 'ril'):
            # Check for settings: geo_layer_origname
            if not type[fields[1]]:
                raise ValidationError(
                    _(f"The config \"{fields[1]}\" parameter for type \"{type['desc']}\" is not set!"))

            # Check for settings: vertex_layer_origname
            if not type[fields[2]] and type['geotype'] not in ('Point', 'MultiPoint'):
                raise ValidationError(
                    _(f"The config \"{fields[2]}\" parameter for type \"{type['desc']}\" is not set!"))

        return self.cleaned_data['type']

    def clean_project(self):

        fields = ('signaler_layer_origname', 'geo_layer_origname', 'vertex_layer_origname', 'state_layer_origname')

        # Check only il clean_type pass
        if 'type' in self.cleaned_data:

            type = SIIM_SIGNAL_TYPES[self.cleaned_data['type']]
            project = self.cleaned_data['project']

            # Check for layer by settings
            # ---------------------------
            layerids = get_layer_orignames(self.cleaned_data['type'], project)

            layer_not_found = []
            for f in fields:
                if type[f] and not type[f] in layerids.keys():
                    layer_not_found.append(type[f])

            if len(layer_not_found) > 0:
                raise ValidationError(
                    _(f"The project selected not contains the follow required layers: {','.join(layer_not_found)}"))

            # Check for editing state for layer
            # ---------------------------------
            layerids2 = copy.copy(layerids)
            del(layerids2[type['state_layer_origname']])
            layers = {l.pk: l for l in project.layer_set.filter(qgs_layer_id__in=layerids2.values())}
            editing_layers = {l.layer_id: l for l in G3WEditingLayer.objects.filter(layer_id__in=layers.keys())}

            no_editing_layer = [layers[i].origname for i in list(set(layers.keys()) - set(editing_layers.keys()))]
            if len(no_editing_layer) > 0:
                raise ValidationError(
                    _(f"The follow layers must be in 'editing' state: {','.join(no_editing_layer)}"))

            # Check if relation between geo layer and vertex layer
            if type['geotype']:
                feature_vertex_relation = False
                try:
                    relations = eval(self.cleaned_data['project'].relations)
                except:
                    relations = []
                for r in relations:
                    if r['referencedLayer'] == layerids[type['geo_layer_origname']] and \
                        r['referencingLayer'] == layerids[type['vertex_layer_origname']]:
                        feature_vertex_relation = True

                if not feature_vertex_relation:
                    raise ValidationError(
                        _(f"Is not set a relation between "
                          f"{type['geo_layer_origname']} and {type['vertex_layer_origname']} layers"))

            # Check for user find into layer
            for l in layers.values():
                fields = [f['name'] for f in eval(l.database_columns)]
                if type['user_field'] not in fields:
                    raise ValidationError(f"Layer {l.name} has not '{type['user_field']}' field!")

                # For signal layer che cp field
                if type['signaler_layer_origname'] == l.origname and type['cp_field'] not in fields:
                    raise ValidationError(f"Layer {l.name} has not '{type['cp_field']}' field!")

        return self.cleaned_data['project']

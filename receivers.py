# coding=utf-8
"""Main Signaler IIM signals receiver module.

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-12-22'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'

from django.conf import settings
from django.dispatch import receiver
from django.template import loader
from django.urls import reverse
from django.utils.translation import ugettext, ugettext_lazy as _, get_language
from core.signals import \
    initconfig_plugin_start, \
    pre_update_project, \
    pre_delete_project, \
    load_dashboard_widgets, \
    post_create_maplayerattributes, \
    load_js_modules, \
    after_serialized_project_layer
from core.views import DashboardView
from qdjango.utils.urls import get_map_url
from qdjango.views import QdjangoProjectUpdateView, QdjangoProjectListView
from usersmanage.signals import after_init_user_form, after_save_user_form
from editing_iim.apiurls import BASE_URLS as EDITING_BASE_URLS
from signaler_iim.models import SIIMSignalerProjects, SIIM_SIGNAL_TYPES, SIIMUserExtraData, Stati, SlsbCp
from signaler_iim.utils import get_layer_orignames, help, get_map_url, get_enc_layer_id
from signaler_iim.apiurls import BASE_URLS as VECTOR_BASE_URLS

import json

import logging

logger = logging.getLogger('signaler_iim')


@receiver(initconfig_plugin_start)
def set_initconfig_value(sender, **kwargs):
    """
    Set SISPI WorkSite data for client initconfig api
    """

    # check il project is a Signaler project
    lang = get_language()
    projects = {s.type: s for s in SIIMSignalerProjects.objects.filter(
        lang=settings.SIIM_SIGNAL_DEFAULT_LANGUAGE_PROJECT)}
    projects_lang = {s.type: s for s in SIIMSignalerProjects.objects.filter(lang=lang)}

    # Update default language projects with current language projects
    projects.update(projects_lang)

    projects_byid = {str(p.project_id): p for p in projects.values()}
    if str(kwargs['project']) not in projects_byid.keys():
        return None

    srp = projects_byid[str(kwargs['project'])]


    # Find QGIS layer id
    stype = SIIM_SIGNAL_TYPES[srp.type]
    layerids = get_layer_orignames(srp.type, srp.project)

    # For Stati and language, try to get field to use:
    stati_desc_field = f'desc_{lang}' if f'desc_{lang}' in [f.attname for f in Stati._meta.fields] else 'desc'

    toret = [
        {
            'mode': 'delete',
            'data': [
                'editing'
            ]
        },
        {
            'mode': 'update',
            'data': {
                'signaler_iim': {
                    'gid': f"qdjango:{kwargs['project']}",
                    'urls': {
                        'vector': f"/{VECTOR_BASE_URLS}/api/",
                        'editing': f"/{EDITING_BASE_URLS}/api/",
                        'export': [],
                        'note': f"/{VECTOR_BASE_URLS}/api/note/list/",
                        'ancestor': f"/{VECTOR_BASE_URLS}/api/getancestor/",
                        'signal_type_maps': {}
                    },
                    'fields': {
                            layerids[stype['signaler_layer_origname']]: ['passaggio', stype['note_field']]
                        },
                    'signal_type': srp.type,
                    'signaler_layer_id': layerids[stype['signaler_layer_origname']],
                    'geo_layer_id': layerids[stype['geo_layer_origname']] if stype['geo_layer_origname'] in layerids else None,
                    'vertex_layer_id': layerids[stype['vertex_layer_origname']] if stype['vertex_layer_origname'] in layerids else None,
                    'signaler_field': settings.SIIM_SIGNAL_FIELD,
                    'ab_signal_fields': stype['ab_signal_fields'],
                    'signaler_user_field': stype['user_field'],
                    'states': {s.value: getattr(s, stati_desc_field) for s in Stati.objects.all()},
                    'state_field': 'passaggio',
                    'roles_editing_acl': settings.SIIM_SIGNAL_EDITING_STATE,
                    'every_fields_editing_states': ['1IL', '2DV'],
                    'relation_signal_types': {},
                    'enc_layer_id': get_enc_layer_id(srp.project, settings.SIIM_ENC_LAYER_ORIGNAME)
                }
            }
        }

    ]

    # Add export urls
    if stype['geo_layer_origname']:
        toret[1]['data']['signaler_iim']['urls']['export'].append({
            'label': _('Coordinate Image'),
            'url': reverse('siim-signals-download-img', args=[srp.type])
        })

    toret[1]['data']['signaler_iim']['urls']['export'].append({
        'label': _('Signal sheet'),
        'url': reverse('siim-signals-download-zip', args=['zip', srp.type])
    })
    toret[1]['data']['signaler_iim']['urls']['export'].append({
        'label': _('S57 ShapeFile'),
        'url': reverse('siim-signals-download-zip', args=['s57', srp.type])
    })



    # Add signal_type map urls
    relation_signal_types = {}
    for t, p in projects.items():
        toret[1]['data']['signaler_iim']['urls']['signal_type_maps'][p.type] = get_map_url(p.project)

        # Get project id and layer id for segnalazioni layer for relation signal type
        if t in stype['relation_signal_types'].keys():
            mapoid = get_layer_orignames(t, p.project, 'not_all')
            relation_signal_types[t] = {
                'project_id': p.project_id,
                'layer_id': mapoid[SIIM_SIGNAL_TYPES[t]['signaler_layer_origname']],
            }

            if 'rules' in stype['relation_signal_types'][t]:
                relation_signal_types[t].update({
                    'rules': stype['relation_signal_types'][t]['rules']
                })
    toret[1]['data']['signaler_iim']['relation_signal_types'] = relation_signal_types

    return toret


@receiver(pre_update_project)
def check_project_for_update(sender, **kwargs):
    """
    Check project is going to update.
    """

    if isinstance(sender, QdjangoProjectUpdateView) and 'signaler_iim' in settings.G3WADMIN_LOCAL_MORE_APPS:

        try:
            SIIMSignalerProjects.objects.get(project=kwargs['project'])
            msg = loader.get_template('signaler_iim/messages/check_project_update.html')
            return msg.render(kwargs)

        except:
            pass


@receiver(pre_delete_project)
def check_project_for_delete(sender, **kwargs):
    """
    Check project is going to delete.
    """

    if isinstance(sender, QdjangoProjectListView) and 'signaler_iim' in settings.G3WADMIN_LOCAL_MORE_APPS:

        # get Signaler IIM projects data
        siim_projects = [c.project for c in SIIMSignalerProjects.objects.all()]
        projects = kwargs['projects']

        if len(siim_projects) > 0:
            messages = []
            for project in projects:
                if project in siim_projects:
                    msg = loader.get_template('signaler_iim/messages/check_project_delete.html')
                    messages.append({'project': project, 'message': msg.render({'project': project})})
            if len(messages):
                return messages


@receiver(load_dashboard_widgets)
def dashboard_widget(sender, **kwargs):

    if isinstance(sender, DashboardView):

        if sender.request.user.has_perm('signaler_iim.make_report') or \
            sender.request.user.has_perm('signaler_iim.manage_reports'):

            # Create urls to new signal
            new_signal_list = []
            for st, t in SIIM_SIGNAL_TYPES.items():
                try:
                    prj = SIIMSignalerProjects.get_project(st, lang=get_language())
                    if prj:
                        map_url = get_map_url(prj)
                        new_signal_list.append({
                            'st': t['desc'],
                            'url': f"{map_url}?sid=new"
                        })
                    else:
                        pass
                except:
                    pass

            context = {
                'user': sender.request.user,
                'signal_types': SIIM_SIGNAL_TYPES,
                'new_signal_list': new_signal_list
            }

            widget = loader.get_template('signaler_iim/widgets/dashboard.html')
            return widget.render(context, request=sender.request)


@receiver(load_js_modules)
def get_js_modules(sender, **kwargs):

    return 'signaler_iim/js/widget.js'


@receiver(post_create_maplayerattributes)
def add_help_to_fields(sender, **kwargs):
    """
    To add help section to response API 'data' mode
    And add default value value for field 'capit_porto' of segnalazioni layer
    """

    vector_params = kwargs['vector_params']

    # Get Signal type by project
    signal_type = SIIMSignalerProjects.get_type(sender.layer.project)

    # Check if layer is `signaler_layer_origname`
    signal_layer = False
    if signal_type in SIIM_SIGNAL_TYPES.keys() and \
            sender.layer.origname == SIIM_SIGNAL_TYPES[signal_type]['signaler_layer_origname']:
        signal_layer = True

    if signal_type in SIIM_SIGNAL_TYPES.keys() and signal_type in help:
        for f in vector_params['fields']:
            if f['name'] in help[signal_type]:
                f['help'] = {
                    'message': help[signal_type][f['name']],
                    'visible': False
                }

            # For id_regione
            if signal_layer and f['name'] == 'capit_porto':

                # Get capit porto by user
                try:
                    capit_porto = SlsbCp.objects.get(pk=kwargs['user'].siimuserextradata.cp)
                    f['input']['options']['default'] = capit_porto.pk
                except:
                    pass


@receiver(after_serialized_project_layer)
def change_info_url_wms(sender, **kwargs):
    """
    Change url of IIM Marine WMS service.
    """

    data = {
        'operation_type': 'update',
        'values': {},
    }

    # Check by originame
    #if SIIMSignalerProjects.objects.filter(project=kwargs['layer'].project).exists() and \
    if kwargs['layer'].origname == settings.SIIM_ENC_LAYER_ORIGNAME:

            data['values']['infourl'] = settings.SIIM_ENC_LAYER_INFOURL
            data['values']['infoformat'] = settings.SIIM_ENC_LAYER_INFOFORMAT

    return data
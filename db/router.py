from django.conf import settings
from signaler_iim.models import TB_DEFAULT_ROUTE


class SIIMRouter(object):
    """
    A router to control all database operations on models in the
    auth application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read auth models.
        """
        if model._meta.app_label == 'signaler_iim':
            if model._meta.model_name in TB_DEFAULT_ROUTE:
                return None
            return settings.SIIM_DBNAME
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth models.
        """
        if model._meta.app_label == 'signaler_iim':
            if model._meta.model_name in TB_DEFAULT_ROUTE:
                return None
            return settings.SIIM_DBNAME
        return None

    def allow_relation(self, obj1, obj2, **hints):
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):

        if app_label == 'signaler_iim':
            if model_name in TB_DEFAULT_ROUTE:
                return db == 'default'
            else:
                return db == settings.SIIM_DBNAME
        else:
            if db == settings.SIIM_DBNAME:
                return False
            return None


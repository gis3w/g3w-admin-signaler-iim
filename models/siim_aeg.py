# coding=utf-8
"""" For Obstacles signa type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES

class ABaseAton(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    base_station = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_base_aton'
        
class ATipo(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_tipo'


class ATipoAton(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    tipo = models.CharField(max_length=255, blank=True, null=True)
    descrizione = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_tipo_aton'


class ATipoFondazione(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    fondazione = models.CharField(max_length=255, blank=True, null=True)
    fondale = models.CharField(max_length=255, blank=True, null=True)
    ancoraggio = models.CharField(max_length=255, blank=True, null=True)
    collocazione = models.CharField(max_length=255, blank=True, null=True)
    tipo = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_tipo_fondazione'


class ATipoMsgAton(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    tipo = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_tipo_msg_aton'


class ATipoOdas(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_tipo_odas'


class ATrasformazione(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_trasformazione'



class ASegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField()
    id_rid = models.CharField(max_length=255)
    nome = models.CharField(max_length=255)
    carta_iim = models.SmallIntegerField()
    pe = models.CharField(max_length=255)
    coord_pe = models.CharField(max_length=255)
    id_regione = models.SmallIntegerField()
    id_prov = models.SmallIntegerField()
    id_comune = models.SmallIntegerField()
    capit_porto = models.SmallIntegerField()
    conc = models.CharField(max_length=255)
    pdc = models.CharField(max_length=255)
    titoloconc = models.CharField(max_length=255)
    dist_shore = models.CharField(max_length=255)
    entro_at = models.BooleanField(blank=True, null=True)
    dist_lb = models.SmallIntegerField()
    aree_mp = models.BooleanField(blank=True, null=True)
    dist_amp = models.SmallIntegerField()
    anno_cost = models.SmallIntegerField()
    nazionalit = models.CharField(max_length=255)
    tipo_aer = models.SmallIntegerField()
    tipo_fond = models.SmallIntegerField()
    h_mozzo = models.SmallIntegerField()
    h_torre = models.SmallIntegerField()
    h_tot = models.SmallIntegerField()
    h_torre_bm = models.SmallIntegerField()
    h_torre_am = models.SmallIntegerField()
    h_max_esc = models.SmallIntegerField()
    max_lato = models.SmallIntegerField()
    h_lmm = models.SmallIntegerField()
    colore = models.CharField(max_length=255)
    marca_mod = models.CharField(max_length=255)
    potenza_mw = models.SmallIntegerField()
    pot_nom = models.SmallIntegerField()
    v_cut_in = models.SmallIntegerField()
    v_nom = models.SmallIntegerField()
    v_cut_out = models.SmallIntegerField()
    lun_pale = models.SmallIntegerField()
    diam_pale = models.SmallIntegerField()
    area_spaz = models.SmallIntegerField()
    v_rotaz = models.SmallIntegerField()
    em_sonora = models.SmallIntegerField()
    a_con_cavi = models.CharField(max_length=255)
    tracciato = models.CharField(max_length=255)
    trasformaz = models.SmallIntegerField()
    centr_mare = models.CharField(max_length=255, blank=True, null=True)
    centr_terr = models.CharField(max_length=255)
    planim_pe = models.CharField(max_length=255)
    fondale = models.SmallIntegerField(blank=True, null=True)
    batim_fond = models.BooleanField(blank=True, null=True)
    data_batim = models.DateField(blank=True, null=True)
    inacc3176 = models.BooleanField(blank=True, null=True)
    data_share = models.BooleanField(blank=True, null=True)
    dem_dtm = models.BooleanField(blank=True, null=True)
    ancoraggi = models.BooleanField(blank=True, null=True)
    nr_ancorag = models.SmallIntegerField(blank=True, null=True)
    vis_radar = models.BooleanField(blank=True, null=True)
    rifl_radar = models.BooleanField(blank=True, null=True)
    beacn_trsp = models.BooleanField(blank=True, null=True)
    seg_ident = models.CharField(max_length=255, blank=True, null=True)
    banda_lav = models.CharField(max_length=255, blank=True, null=True)
    periodo = models.SmallIntegerField(blank=True, null=True)
    azimut = models.SmallIntegerField(blank=True, null=True)
    portata = models.SmallIntegerField(blank=True, null=True)
    rds = models.BooleanField(blank=True, null=True)
    ais_aton = models.BooleanField(blank=True, null=True)
    sbs = models.SmallIntegerField(blank=True, null=True)
    tipo_aton = models.SmallIntegerField(blank=True, null=True)
    tipo_msg = models.SmallIntegerField(blank=True, null=True)
    dsc_vhf = models.BooleanField(blank=True, null=True)
    id_mmsi = models.IntegerField()
    dsc_mf = models.BooleanField(blank=True, null=True)
    seg_lum = models.BooleanField(blank=True, null=True)
    fari_neb = models.BooleanField(blank=True, null=True)
    foa = models.BooleanField(blank=True, null=True)
    boeproslum = models.BooleanField(blank=True, null=True)
    boeprosdiu = models.BooleanField(blank=True, null=True)
    noteboediu = models.CharField(max_length=255, blank=True, null=True)
    odas = models.BooleanField(blank=True, null=True)
    boa_odas = models.BooleanField(blank=True, null=True)
    staz_odas = models.BooleanField(blank=True, null=True)
    tipo_odas = models.SmallIntegerField(blank=True, null=True)
    note_odas = models.CharField(max_length=255, blank=True, null=True)
    api = models.BooleanField(blank=True, null=True)
    api_online = models.CharField(max_length=255, blank=True, null=True)
    free_acces = models.BooleanField(blank=True, null=True)
    account = models.BooleanField(blank=True, null=True)
    username = models.CharField(max_length=255, blank=True, null=True)
    psw = models.CharField(max_length=255, blank=True, null=True)
    ord_am = models.BooleanField(blank=True, null=True)
    ordinanze = models.CharField(max_length=255, blank=True, null=True)
    divieti = models.CharField(max_length=255)
    prescriz = models.CharField(max_length=255)
    norme_prec = models.CharField(max_length=255)
    foto = models.CharField(max_length=255)
    annotazioni = models.CharField(max_length=255)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['aeg']['model'] = ASegnalazioni


class ASignalFeatures(models.Model):
    signal = models.ForeignKey(ASegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'a_signal_features'



# Set for signal stype settings
SIIM_SIGNAL_TYPES['aeg']['geo_model'] = ASignalFeatures


class ASignalVertex(models.Model):
    feature = models.ForeignKey(ASignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'a_signal_vertex'
    
    
# Set for signal stype settings
SIIM_SIGNAL_TYPES['aeg']['vx_model'] = ASignalVertex
        





# ==============================================
# View to cumulate ids(pks)
# ==============================================
class ASignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'a_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['aeg']['indexes_model'] = ASignalPassaggioPk



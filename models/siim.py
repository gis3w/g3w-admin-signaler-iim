# coding=utf-8
""""
    Singaler IIM models (only read) geo data.
.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-12-22'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'


from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES


class SlsSegnalazioni(models.Model):
    tipo_scheda = models.CharField(max_length=255, blank=True, null=True)
    presc_marifari = models.CharField(max_length=255, blank=True, null=True)
    conf_presc = models.CharField(max_length=255, blank=True, null=True)
    num_ef = models.CharField(max_length=255, blank=True, null=True)
    regione = models.CharField(max_length=255, blank=True, null=True)
    provincia = models.CharField(max_length=255, blank=True, null=True)
    comune = models.IntegerField(blank=True, null=True)
    mare = models.CharField(max_length=255, blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    localita = models.CharField(max_length=255, blank=True, null=True)
    ubicazione = models.CharField(max_length=255, blank=True, null=True)
    tipo_segn = models.CharField(max_length=255, blank=True, null=True)
    altro_tipo_segn = models.CharField(max_length=255, blank=True, null=True)
    caratt_luce = models.CharField(max_length=255, blank=True, null=True)
    col_luce = models.CharField(max_length=255, blank=True, null=True)
    periodo = models.IntegerField(blank=True, null=True)
    luce_eclissi = models.CharField(max_length=255, blank=True, null=True)
    struttura = models.CharField(max_length=255, blank=True, null=True)
    col_struttura = models.CharField(max_length=255, blank=True, null=True)
    portata_lum = models.IntegerField(blank=True, null=True)
    alt_luce = models.IntegerField(blank=True, null=True)
    alt_strutt = models.IntegerField(blank=True, null=True)
    visibilita = models.CharField(max_length=255, blank=True, null=True)
    col_settore = models.CharField(max_length=255, blank=True, null=True)
    miraglio = models.CharField(max_length=255, blank=True, null=True)
    tipo_miraglio = models.CharField(max_length=255, blank=True, null=True)
    riflett_radar = models.CharField(max_length=255, blank=True, null=True)
    note_fasi_settore = models.CharField(max_length=255, blank=True, null=True)
    segn_nebbia = models.CharField(max_length=255, blank=True, null=True)
    tipo_segn_nebbia = models.CharField(max_length=255, blank=True, null=True)
    periodo_segn_nebbia = models.CharField(max_length=255, blank=True, null=True)
    fase_segn_nebbia = models.CharField(max_length=255, blank=True, null=True)
    segn_morse = models.CharField(max_length=255, blank=True, null=True)
    portata_segn_nebbia = models.CharField(max_length=255, blank=True, null=True)
    stato = models.CharField(max_length=255, blank=True, null=True)
    soggetto_spegn = models.CharField(max_length=255, blank=True, null=True)
    avurnav = models.CharField(max_length=255, blank=True, null=True)
    nr_avurnav = models.CharField(max_length=255, blank=True, null=True)
    marifari_comp = models.CharField(max_length=255, blank=True, null=True)
    tipo_concess = models.CharField(max_length=255, blank=True, null=True)
    concess_marifari = models.CharField(max_length=255, blank=True, null=True)
    concess_privato = models.CharField(max_length=255, blank=True, null=True)
    contatto = models.IntegerField(blank=True, null=True)
    titolo_concessorio = models.CharField(max_length=255, blank=True, null=True)
    foto = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.CharField(max_length=255, blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sls_segnalazioni'

    @classmethod
    def total(cls):
        """ Return total number of signals"""

        return cls.objects.all().count()


# Set for signal stype settings
SIIM_SIGNAL_TYPES['tes']['model'] = SlsSegnalazioni


class SlsSignalFeatures(models.Model):
    signal_id = models.IntegerField()
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPolygonField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sls_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['tes']['geo_model'] = SlsSignalFeatures


class SlsSignalVertex(models.Model):
    feature_id = models.IntegerField()
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sls_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['tes']['vx_model'] = SlsSignalVertex


# ==============================================
# View to cumulate ids(pks)
# ==============================================
class SlsSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'sls_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['tes']['indexes_model'] = SlsSignalPassaggioPk


class SlsbColor(models.Model):
    id = models.IntegerField(primary_key=True)
    colore = models.CharField(max_length=255, blank=True, null=True)
    descrizione = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_color'


class SlsbComune(models.Model):
    id = models.IntegerField(primary_key=True)
    id_mare = models.IntegerField(blank=True, null=True)
    id_reg = models.IntegerField(blank=True, null=True)
    id_prov = models.IntegerField(blank=True, null=True)
    comune = models.CharField(max_length=255, blank=True, null=True)
    id_adsp = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_comune'


class SlsbConcmarifari(models.Model):
    id = models.IntegerField(primary_key=True)
    conc_marifari = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_concmarifari'


class SlsbCp(models.Model):
    id = models.IntegerField(primary_key=True)
    id_loc = models.CharField(max_length=255, blank=True, null=True)
    id_comune = models.CharField(max_length=255, blank=True, null=True)
    id_categoria = models.IntegerField(blank=True, null=True)
    id_regione = models.IntegerField(blank=True, null=True)

    def __str__(self):
        try:
            return self.id_loc
        except:
            return ''

    class Meta:
        managed = False
        db_table = 'slsb_cp'


class SlsbMare(models.Model):
    id = models.IntegerField(primary_key=True)
    mare = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_mare'


class SlsbMarifari(models.Model):
    id = models.IntegerField(primary_key=True)
    marifari = models.CharField(max_length=255, blank=True, null=True)
    descrizione = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_marifari'


class SlsbProvincia(models.Model):
    id = models.IntegerField(primary_key=True)
    id_reg = models.IntegerField(blank=True, null=True)
    provincia = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_provincia'


class SlsbRegione(models.Model):
    id = models.IntegerField(primary_key=True)
    regione = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_regione'


class SlsbSchede(models.Model):
    id = models.IntegerField(primary_key=True)
    scheda = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_schede'


class SlsbSeglum(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo_segnalamento = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_seglum'


class SlsbSegmorse(models.Model):
    id = models.IntegerField(primary_key=True)
    segnale_morse = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_segmorse'


class SlsbStatoseg(models.Model):
    id = models.IntegerField(primary_key=True)
    stato_segnalamento = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_statoseg'


class SlsbTipoCarlum(models.Model):
    id = models.IntegerField(primary_key=True)
    luce = models.CharField(max_length=255, blank=True, null=True)
    descrizione = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_tipo_carlum'


class SlsbTipoConcessorio(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo_concessorio = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_tipo_concessorio'


class SlsbTipoFondale(models.Model):
    id = models.IntegerField(primary_key=True)
    natura_fondale = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_tipo_fondale'


class SlsbTipoMiragli(models.Model):
    id = models.IntegerField(primary_key=True)
    miragli = models.CharField(max_length=255, blank=True, null=True)
    simbolo = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_tipo_miragli'


class SlsbTipoScheda(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo_scheda = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_tipo_scheda'


class SlsbTipoStrumsonori(models.Model):
    id = models.IntegerField(primary_key=True)
    sound_prod_system = models.CharField(max_length=255, blank=True, null=True)
    appar_sonore = models.CharField(max_length=255, blank=True, null=True)
    descrizione = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_tipo_strumsonori'


class SlsbTipoStrutseg(models.Model):
    id = models.IntegerField(primary_key=True)
    strutt_segn = models.CharField(max_length=255, blank=True, null=True)
    structure = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'slsb_tipo_strutseg'


class Stati(models.Model):
    value = models.CharField(max_length=255)
    desc = models.CharField(max_length=255)
    desc_en = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    color = models.CharField(max_length=255, blank=True)

    @classmethod
    def get_value(cls, desc):
        """ Give desc value return 'value value' """
        return cls.objects.get(desc=desc).value

    class Meta:
        managed = False
        db_table = 'stati'


class SSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField(blank=True, null=True)
    presc_marifari = models.CharField(max_length=255, blank=True, null=True)
    conf_presc = models.SmallIntegerField(blank=True, null=True)
    num_ef = models.CharField(max_length=255, blank=True, null=True)
    id_regione = models.SmallIntegerField()
    id_prov = models.SmallIntegerField()
    id_comune = models.SmallIntegerField()
    capit_porto = models.SmallIntegerField()
    mare = models.SmallIntegerField(blank=True, null=True)
    localita = models.CharField(max_length=255, blank=True, null=True)
    ubicazione = models.CharField(max_length=255, blank=True, null=True)
    tipo_segn = models.SmallIntegerField(blank=True, null=True)
    altro_tipo_segn = models.CharField(max_length=255, blank=True, null=True)
    caratt_luce = models.SmallIntegerField(blank=True, null=True)
    col_luce = models.SmallIntegerField(blank=True, null=True)
    periodo = models.IntegerField(blank=True, null=True)
    luce_eclissi = models.CharField(max_length=255, blank=True, null=True)
    struttura = models.SmallIntegerField(blank=True, null=True)
    col_struttura = models.SmallIntegerField(blank=True, null=True)
    portata_lum = models.IntegerField(blank=True, null=True)
    alt_luce = models.IntegerField(blank=True, null=True)
    alt_strutt = models.IntegerField(blank=True, null=True)
    visibilita = models.CharField(max_length=255, blank=True, null=True)
    col_settore = models.SmallIntegerField(blank=True, null=True)
    miraglio = models.SmallIntegerField(blank=True, null=True)
    tipo_miraglio = models.SmallIntegerField(blank=True, null=True)
    riflett_radar = models.SmallIntegerField(blank=True, null=True)
    note_fasi_settore = models.CharField(max_length=255, blank=True, null=True)
    segn_nebbia = models.SmallIntegerField(blank=True, null=True)
    tipo_segn_nebbia = models.SmallIntegerField(blank=True, null=True)
    periodo_segn_nebbia = models.CharField(max_length=255, blank=True, null=True)
    fase_segn_nebbia = models.CharField(max_length=255, blank=True, null=True)
    segn_morse = models.SmallIntegerField(blank=True, null=True)
    portata_segn_nebbia = models.CharField(max_length=255, blank=True, null=True)
    stato = models.SmallIntegerField(blank=True, null=True)
    soggetto_spegn = models.SmallIntegerField(blank=True, null=True)
    avurnav = models.SmallIntegerField(blank=True, null=True)
    nr_avurnav = models.CharField(max_length=255, blank=True, null=True)
    marifari_comp = models.SmallIntegerField(blank=True, null=True)
    tipo_concess = models.SmallIntegerField(blank=True, null=True)
    concess_marifari = models.SmallIntegerField(blank=True, null=True)
    concess_privato = models.CharField(max_length=255, blank=True, null=True)
    contatto = models.IntegerField(blank=True, null=True)
    titolo_concessorio = models.CharField(max_length=255, blank=True, null=True)
    foto = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.CharField(max_length=255, blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    chiusura = models.CharField(max_length=255, blank=True, null=True)
    archiviazione = models.CharField(max_length=255, blank=True, null=True)
    commenti = models.CharField(max_length=255, blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 's_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['sls']['model'] = SSegnalazioni


class SSignalFeatures(models.Model):
    signal = models.ForeignKey(SSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 's_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['sls']['geo_model'] = SSignalFeatures


class SSignalVertex(models.Model):
    feature = models.ForeignKey(SSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 's_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['sls']['vx_model'] = SSignalVertex


# ==============================================
# View to cumulate ids(pks)
# ==============================================
class SSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 's_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['sls']['indexes_model'] = SSignalPassaggioPk
# coding=utf-8
"""" For Port works signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES



class LpAdsp(models.Model):
    id = models.IntegerField(primary_key=True)
    nome = models.CharField(max_length=255, blank=True, null=True)
    sede = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lp_adsp'


class LpAdspPorti(models.Model):
    id = models.IntegerField(primary_key=True)
    id_adsp = models.IntegerField(blank=True, null=True)
    nome = models.CharField(max_length=255, blank=True, null=True)
    codice = models.CharField(max_length=255, blank=True, null=True)
    id_comune = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lp_adsp_porti'


class LpSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField()
    id_regione = models.SmallIntegerField()
    id_prov = models.SmallIntegerField()
    id_comune = models.SmallIntegerField()
    comp_lp = models.SmallIntegerField()
    id_adsp = models.SmallIntegerField(blank=True, null=True)
    id_porto = models.SmallIntegerField(blank=True, null=True)
    concession = models.CharField(max_length=255, blank=True, null=True)
    localita = models.CharField(max_length=255)
    tipo = models.SmallIntegerField()
    plan_all = models.CharField(max_length=255)
    data_init = models.DateField()
    lotti = models.CharField(max_length=255, blank=True, null=True)
    data_fine = models.DateField()
    appaltator = models.SmallIntegerField()
    info_appal = models.CharField(max_length=255)
    zona_reg = models.SmallIntegerField(blank=True, null=True)
    all_ord = models.CharField(max_length=255, blank=True, null=True)
    id_ord = models.CharField(max_length=255, blank=True, null=True)
    seg_lum = models.SmallIntegerField(blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lp_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['pws']['model'] = LpSegnalazioni


class LpSignalFeatures(models.Model):
    signal = models.ForeignKey(LpSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPolygonField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'lp_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['pws']['geo_model'] = LpSignalFeatures


class LpSignalVertex(models.Model):
    feature = models.ForeignKey(LpSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lp_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['pws']['vx_model'] = LpSignalVertex



# ==============================================
# View to cumulate ids(pks)
# ==============================================
class PwsSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'lp_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['pws']['indexes_model'] = PwsSignalPassaggioPk





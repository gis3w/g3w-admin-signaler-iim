# coding=utf-8
"""" For Platforms signal type RIL (Rilievi/Survey)

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES




class RilSegnalazioni(models.Model):
    desc_scheda = models.CharField(max_length=400)
    ref_scheda = models.SmallIntegerField()
    aut_iim = models.CharField(max_length=255)
    prot_iim = models.CharField(max_length=255)
    data_aut = models.DateField()
    rel_tec = models.SmallIntegerField(blank=True, null=True)
    calibraz = models.SmallIntegerField(blank=True, null=True)
    misure_bat = models.SmallIntegerField(blank=True, null=True)
    elab_dati = models.SmallIntegerField(blank=True, null=True)
    misure_geo = models.SmallIntegerField(blank=True, null=True)
    misure_top = models.SmallIntegerField(blank=True, null=True)
    ortofoto = models.SmallIntegerField(blank=True, null=True)
    misure_mar = models.SmallIntegerField(blank=True, null=True)
    correzioni = models.SmallIntegerField(blank=True, null=True)
    misure_vel = models.SmallIntegerField(blank=True, null=True)
    backscatter = models.SmallIntegerField(blank=True, null=True)
    logfile = models.SmallIntegerField(blank=True, null=True)
    dati_mappe = models.SmallIntegerField(blank=True, null=True)
    reg_geofis = models.SmallIntegerField(blank=True, null=True)
    brogliacci = models.SmallIntegerField(blank=True, null=True)
    osservaz = models.SmallIntegerField(blank=True, null=True)
    ecogrammi = models.SmallIntegerField(blank=True, null=True)
    mappe = models.SmallIntegerField(blank=True, null=True)
    link = models.CharField(max_length=400, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ril_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['ril']['model'] = RilSegnalazioni


# ==============================================
# View to cumulate ids(pks)
# ==============================================
class RilSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'ril_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['ril']['indexes_model'] = RilSignalPassaggioPk



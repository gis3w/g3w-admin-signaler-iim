# coding=utf-8
"""" For Platforms signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES


class PfSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField()
    id_mise = models.SmallIntegerField()
    id_rid = models.CharField(max_length=255)
    nome = models.CharField(max_length=255)
    anno_cost = models.SmallIntegerField()
    utilizzo = models.SmallIntegerField()
    tipo_strut = models.SmallIntegerField()
    tipo = models.SmallIntegerField()
    colore = models.CharField(max_length=255)
    fiamma = models.BooleanField(blank=True, null=True)
    nazionalit = models.CharField(max_length=255)
    stato = models.SmallIntegerField()
    zona = models.SmallIntegerField()
    sez_unmig = models.SmallIntegerField()
    carta_iim = models.SmallIntegerField()
    id_regione = models.SmallIntegerField()
    id_prov = models.BooleanField()
    id_comune = models.SmallIntegerField()
    capit_porto = models.SmallIntegerField()
    minerale = models.SmallIntegerField(blank=True, null=True)
    concession = models.SmallIntegerField()
    pdc = models.CharField(max_length=255)
    titoloconc = models.CharField(max_length=255)
    titolomin = models.CharField(max_length=255)
    pozzi = models.BooleanField(blank=True, null=True)
    n_pozzi = models.SmallIntegerField(blank=True, null=True)
    supporto = models.BooleanField(blank=True, null=True)
    centrale = models.SmallIntegerField(blank=True, null=True)
    coll_centr = models.BooleanField(blank=True, null=True)
    coll_attivo = models.BooleanField(blank=True, null=True)
    coll_altre = models.BooleanField(blank=True, null=True)
    altre_pf = models.CharField(max_length=255, blank=True, null=True)
    dist_costa = models.IntegerField(blank=True, null=True)
    in_acque_t = models.BooleanField(blank=True, null=True)
    dist_lbase = models.IntegerField(blank=True, null=True)
    aree_prot = models.BooleanField(blank=True, null=True)
    dist_amp = models.IntegerField(blank=True, null=True)
    h_lmm = models.SmallIntegerField(blank=True, null=True)
    vis_radar = models.BooleanField(blank=True, null=True)
    rifl_radar = models.BooleanField(blank=True, null=True)
    beacn_trsp = models.BooleanField(blank=True, null=True)
    seg_ident = models.CharField(max_length=255, blank=True, null=True)
    banda_lav = models.CharField(max_length=255, blank=True, null=True)
    periodo = models.SmallIntegerField(blank=True, null=True)
    azimut = models.SmallIntegerField(blank=True, null=True)
    portata = models.SmallIntegerField(blank=True, null=True)
    efficienza = models.SmallIntegerField(blank=True, null=True)
    rds = models.BooleanField(blank=True, null=True)
    ais_aton = models.BooleanField(blank=True, null=True)
    id_mmsi = models.IntegerField()
    sbs = models.SmallIntegerField(blank=True, null=True)
    tipo_aton = models.SmallIntegerField(blank=True, null=True)
    tipo_msg = models.SmallIntegerField(blank=True, null=True)
    dsc_vhf = models.BooleanField(blank=True, null=True)
    dsc_mf = models.BooleanField(blank=True, null=True)
    seg_lum = models.BooleanField(blank=True, null=True)
    fari_neb = models.BooleanField(blank=True, null=True)
    foa = models.BooleanField(blank=True, null=True)
    boeproslum = models.BooleanField(blank=True, null=True)
    boeprosdiu = models.BooleanField(blank=True, null=True)
    noteboediu = models.CharField(max_length=255, blank=True, null=True)
    odas = models.BooleanField(blank=True, null=True)
    tipo_odas = models.SmallIntegerField(blank=True, null=True)
    note_odas = models.CharField(max_length=255, blank=True, null=True)
    api = models.BooleanField(blank=True, null=True)
    api_online = models.CharField(max_length=255, blank=True, null=True)
    free_acces = models.BooleanField(blank=True, null=True)
    account = models.BooleanField(blank=True, null=True)
    username = models.BooleanField(blank=True, null=True)
    psw = models.CharField(max_length=255, blank=True, null=True)
    odas_off = models.BooleanField(blank=True, null=True)
    divieti = models.CharField(max_length=255)
    prescriz = models.CharField(max_length=255)
    norme_prec = models.CharField(max_length=255)
    ord_am = models.BooleanField(blank=True, null=True)
    ordinanze = models.CharField(max_length=255, blank=True, null=True)
    helipad = models.BooleanField(blank=True, null=True)
    tipo_eli = models.CharField(max_length=255, blank=True, null=True)
    hpad_lmm = models.SmallIntegerField(blank=True, null=True)
    hpad_estens = models.IntegerField(blank=True, null=True)
    dist_ostac = models.IntegerField(blank=True, null=True)
    work_freq = models.CharField(max_length=255, blank=True, null=True)
    presidiata = models.BooleanField(blank=True, null=True)
    alloggi = models.CharField(max_length=255, blank=True, null=True)
    max_n_pers = models.SmallIntegerField(blank=True, null=True)
    avg_n_pers = models.SmallIntegerField(blank=True, null=True)
    rotte = models.CharField(max_length=255, blank=True, null=True)
    porti = models.CharField(max_length=255, blank=True, null=True)
    ormeggio = models.BooleanField(blank=True, null=True)
    limiti = models.CharField(max_length=255, blank=True, null=True)
    elettricita = models.BooleanField(blank=True, null=True)
    tipo_gen = models.CharField(max_length=255, blank=True, null=True)
    num_gen = models.SmallIntegerField(blank=True, null=True)
    n_gen_sos = models.SmallIntegerField(blank=True, null=True)
    potenza = models.FloatField(blank=True, null=True)
    voltaggio = models.FloatField(blank=True, null=True)
    frequenza = models.FloatField(blank=True, null=True)
    batim_fond = models.BooleanField(blank=True, null=True)
    data_batim = models.DateField(blank=True, null=True)
    inacc3176 = models.BooleanField(blank=True, null=True)
    data_share = models.BooleanField(blank=True, null=True)
    dem_dtm = models.BooleanField(blank=True, null=True)
    ancoraggi = models.BooleanField(blank=True, null=True)
    nr_ancorag = models.SmallIntegerField(blank=True, null=True)
    planimetr = models.CharField(max_length=255, blank=True, null=True)
    foto = models.CharField(max_length=255)
    annotazioni = models.CharField(max_length=255)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    tracciato = models.CharField(max_length=255, blank=True, null=True)
    tracc_altr = models.CharField(max_length=255, blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pf_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['plt']['model'] = PfSegnalazioni



class PfSignalFeatures(models.Model):
    signal = models.ForeignKey(PfSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'pf_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['plt']['geo_model'] = PfSignalFeatures


class PfSignalVertex(models.Model):
    feature = models.ForeignKey(PfSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pf_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['plt']['vx_model'] = PfSignalVertex


class PfStruttura(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    struttura = models.CharField(max_length=255, blank=True, null=True)
    id_utilizzo = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pf_struttura'


class PfTipo(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pf_tipo'


class PfUtilizzo(models.Model):
    id = models.IntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pf_utilizzo'


# ==============================================
# View to cumulate ids(pks)
# ==============================================
class PltSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'pf_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['plt']['indexes_model'] = PltSignalPassaggioPk



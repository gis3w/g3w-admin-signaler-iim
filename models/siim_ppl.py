# coding=utf-8
"""" For Platforms signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES



class PplPerimetroLineare(models.Model):
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    note = models.CharField(max_length=5, blank=True, null=True)
    tipo_scheda = models.SmallIntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ppl_perimetro_lineare'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['ppl']['model'] = PplPerimetroLineare


class PplSignalFeatures(models.Model):
    tipo_scheda = models.SmallIntegerField()
    signal = models.ForeignKey(PplPerimetroLineare, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=400, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiLineStringField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.
    note = models.CharField(max_length=400, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ppl_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['ppl']['geo_model'] = PplSignalFeatures


class PplSignalVertex(models.Model):
    feature = models.ForeignKey(PplSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=400, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ppl_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['ppl']['vx_model'] =  PplSignalVertex



# ==============================================
# View to cumulate ids(pks)
# ==============================================
class PplSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'ppl_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['ppl']['indexes_model'] = PplSignalPassaggioPk



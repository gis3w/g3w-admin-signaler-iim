# coding=utf-8
"""" For Wells signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES



class PCampo(models.Model):
    id = models.IntegerField(primary_key=True)
    nome_campo = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_campo'


class PCentrale(models.Model):
    id = models.IntegerField(primary_key=True)
    nome = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_centrale'


class PConcessionaria(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    concessionario = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_concessionaria'


class PMinerale(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_minerale'


class PSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField(blank=True, null=True)
    id_regione = models.SmallIntegerField()
    id_prov = models.SmallIntegerField()
    id_comune = models.SmallIntegerField()
    capit_porto = models.SmallIntegerField()
    id_piattaf = models.SmallIntegerField(blank=True, null=True)
    identif = models.CharField(max_length=255, blank=True, null=True)
    nome_pozzo = models.CharField(max_length=255, blank=True, null=True)
    prodotto = models.SmallIntegerField(blank=True, null=True)
    stato = models.SmallIntegerField()
    zona = models.SmallIntegerField()
    sez_unmig = models.SmallIntegerField()
    carta_iim = models.SmallIntegerField()
    campo = models.SmallIntegerField(blank=True, null=True)
    centrale = models.SmallIntegerField(blank=True, null=True)
    concession = models.SmallIntegerField()
    pdc = models.CharField(max_length=255)
    titolomin = models.CharField(max_length=255)
    collegam = models.BooleanField(blank=True, null=True)
    dist_costa = models.SmallIntegerField(blank=True, null=True)
    in_acque_t = models.BooleanField(blank=True, null=True)
    dist_lbase = models.SmallIntegerField(blank=True, null=True)
    aree_prot = models.BooleanField(blank=True, null=True)
    dist_amp = models.IntegerField(blank=True, null=True)
    profondita = models.SmallIntegerField(blank=True, null=True)
    altezza = models.SmallIntegerField(blank=True, null=True)
    battente = models.SmallIntegerField(blank=True, null=True)
    dim_valv = models.CharField(max_length=255, blank=True, null=True)
    dim_atvalv = models.CharField(max_length=255, blank=True, null=True)
    tipo_a = models.CharField(max_length=255, blank=True, null=True)
    batimetria = models.BooleanField(blank=True, null=True)
    data_batim = models.DateField(blank=True, null=True)
    ii_3176 = models.BooleanField(blank=True, null=True)
    dati_share = models.BooleanField(blank=True, null=True)
    dem_dtm = models.BooleanField(blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['wel']['model'] = PSegnalazioni


class PSignalFeatures(models.Model):
    signal = models.ForeignKey(PSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'p_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['wel']['geo_model'] = PSignalFeatures


class PSignalVertex(models.Model):
    feature = models.ForeignKey(PSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['wel']['vx_model'] = PSignalVertex


class PStatoPiattaforma(models.Model):
    id = models.IntegerField(primary_key=True)
    sigla_stato = models.CharField(max_length=255, blank=True, null=True)
    stato = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_stato_piattaforma'


class PZona(models.Model):
    zona = models.CharField(max_length=255, blank=True, null=True)
    ufficio = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_zona'


class PZonaUfficio(models.Model):
    id_zona = models.CharField(max_length=255, blank=True, null=True)
    ufficio = models.CharField(max_length=255, blank=True, null=True)
    zona = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'p_zona_ufficio'


# ==============================================
# View to cumulate ids(pks)
# ==============================================
class WelSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'p_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['wel']['indexes_model'] = WelSignalPassaggioPk



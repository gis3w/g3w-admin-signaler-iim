# coding=utf-8
"""" For Nutiche informations  signa type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES


class InCarta(models.Model):
    nr_carta = models.CharField(max_length=255, blank=True, null=True)
    titolo = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'in_carta'


class InCpMare(models.Model):
    field_id_cp_in_field = models.IntegerField(db_column='"id_cp_in"', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_nr_cp_in_field = models.CharField(db_column='"nr_cp_in"', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_cp_in_field = models.CharField(db_column='"cp_in"', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_id_marein_field = models.IntegerField(db_column='"id_marein"', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_mare_in_field = models.CharField(db_column='"mare_in"', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_descrizione_field = models.CharField(db_column='"descrizione"', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'in_cp_mare'


class InMare(models.Model):
    field_sigla_mare_field = models.CharField(db_column='"sigla_mare"', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_mare_int_field = models.CharField(db_column='"mare_int"', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.
    field_descrizione_field = models.CharField(db_column='"descrizione"', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'in_mare'


class InPubblicazione(models.Model):
    field_pubblicazione_field = models.CharField(db_column='"pubblicazione"', max_length=255, blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'. Field renamed because it ended with '_'.

    class Meta:
        managed = False
        db_table = 'in_pubblicazione'


class InSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField()
    titolo_mar = models.SmallIntegerField()
    acque_int = models.BooleanField(blank=True, null=True)
    cp_in = models.SmallIntegerField(blank=True, null=True)
    nr_in = models.CharField(max_length=255, blank=True, null=True)
    anno = models.SmallIntegerField()
    localit_in = models.CharField(max_length=255, blank=True, null=True)
    titolo_in = models.CharField(max_length=255, blank=True, null=True)
    tipo_in = models.CharField(max_length=255, blank=True, null=True)
    testo = models.CharField(max_length=255, blank=True, null=True)
    avurnav = models.BooleanField(blank=True, null=True)
    nr_avurnav = models.CharField(max_length=255, blank=True, null=True)
    fonte = models.CharField(max_length=255, blank=True, null=True)
    info_fonte = models.CharField(max_length=255, blank=True, null=True)
    carta1 = models.CharField(max_length=255, blank=True, null=True)
    carta2 = models.CharField(max_length=255, blank=True, null=True)
    carta3 = models.CharField(max_length=255, blank=True, null=True)
    carta4 = models.CharField(max_length=255, blank=True, null=True)
    carta5 = models.CharField(max_length=255, blank=True, null=True)
    carta6 = models.CharField(max_length=255, blank=True, null=True)
    pubblicaz1 = models.CharField(max_length=255, blank=True, null=True)
    pubblicaz2 = models.CharField(max_length=255, blank=True, null=True)
    pubblicaz3 = models.CharField(max_length=255, blank=True, null=True)
    nr_scheda = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'in_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['iin']['model'] = InSegnalazioni


class InSignalFeatures(models.Model):
    signal = models.ForeignKey(InSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'in_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['iin']['geo_model'] = InSignalFeatures


class InSignalVertex(models.Model):
    feature = models.ForeignKey(InSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'in_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['iin']['vx_model'] = InSignalVertex


class InTipo(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    tipo = models.CharField(max_length=255, blank=True, null=True)
    descrizione = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'in_tipo'


# ==============================================
# View to cumulate ids(pks)
# ==============================================
class IinSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'in_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['iin']['indexes_model'] = IinSignalPassaggioPk



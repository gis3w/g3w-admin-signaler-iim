# coding=utf-8
"""" For Obstacles signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES



class ODetFondaleBattente(models.Model):
    id = models.IntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'o_det_fondale_battente'


class OMateriale(models.Model):
    id = models.IntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'o_materiale'


class OModalitaRilevamento(models.Model):
    id = models.IntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'o_modalita_rilevamento'


class OSegnalazioni(models.Model):
    id_regione = models.SmallIntegerField(blank=True, null=True)
    id_prov = models.SmallIntegerField(blank=True, null=True)
    id_comune = models.SmallIntegerField(blank=True, null=True)
    localita = models.CharField(max_length=255, blank=True, null=True)
    tipo_ost = models.CharField(max_length=255, blank=True, null=True)
    desc = models.CharField(max_length=255, blank=True, null=True)
    mod_ril = models.CharField(max_length=255, blank=True, null=True)
    battente = models.IntegerField(blank=True, null=True)
    fondale = models.IntegerField(blank=True, null=True)
    det_fond = models.CharField(max_length=255, blank=True, null=True)
    tipo_fond = models.CharField(max_length=255, blank=True, null=True)
    det_pos = models.CharField(max_length=255, blank=True, null=True)
    coord_estr = models.CharField(max_length=255, blank=True, null=True)
    est_rag = models.IntegerField(blank=True, null=True)
    est_lun = models.IntegerField(blank=True, null=True)
    est_lar = models.IntegerField(blank=True, null=True)
    materiale = models.CharField(max_length=255, blank=True, null=True)
    altro_mat = models.CharField(max_length=255, blank=True, null=True)
    planimetr = models.BooleanField(blank=True, null=True)
    plan_all = models.CharField(max_length=255, blank=True, null=True)
    seg_lum = models.BooleanField(blank=True, null=True)
    divieti = models.BooleanField(blank=True, null=True)
    ord_all = models.CharField(max_length=255, blank=True, null=True)
    id_ord = models.CharField(max_length=255, blank=True, null=True)
    avurnav = models.BooleanField(blank=True, null=True)
    nr_avurnav = models.CharField(max_length=255, blank=True, null=True)
    data = models.CharField(max_length=255, blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'o_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['obs']['model'] = OSegnalazioni


class OSignalFeatures(models.Model):
    signal = models.ForeignKey(OSegnalazioni, models.DO_NOTHING, blank=True, null=True)
    geom = models.MultiPointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'o_signal_features'

# Set for signal stype settings
SIIM_SIGNAL_TYPES['obs']['geo_model'] = OSignalFeatures


class OSignalVertex(models.Model):
    id = models.IntegerField(primary_key=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    feature_id = models.IntegerField(blank=True, null=True)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'o_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['obs']['vx_model'] = OSignalVertex

# ==============================================
# View to cumulate ids(pks)
# ==============================================
class ObsSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'obs_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['obs']['indexes_model'] = ObsSignalPassaggioPk



class OTipoOstacolo(models.Model):
    id = models.IntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'o_tipo_ostacolo'

# coding=utf-8
"""" For Planimetries signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES


class PlSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField()
    id_regione = models.SmallIntegerField()
    id_prov = models.SmallIntegerField()
    id_comune = models.SmallIntegerField()
    localita = models.CharField(max_length=255)
    plan_all = models.CharField(max_length=255)
    data_val = models.DateField()
    trasmiss = models.BooleanField(blank=True, null=True)
    id_trasmis = models.CharField(max_length=255, blank=True, null=True)
    varianti = models.BooleanField(blank=True, null=True)
    id_var = models.CharField(max_length=255, blank=True, null=True)
    zona_reg = models.BooleanField(blank=True, null=True)
    all_ord = models.CharField(max_length=255, blank=True, null=True)
    id_ord = models.CharField(max_length=255, blank=True, null=True)
    aann = models.BooleanField(blank=True, null=True)
    id_aann = models.CharField(max_length=255, blank=True, null=True)
    id_av = models.SmallIntegerField(blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pl_segnalazioni'



# Set for signal stype settings
SIIM_SIGNAL_TYPES['pla']['model'] = PlSegnalazioni


# ==============================================
# View to cumulate ids(pks)
# ==============================================
class PlaSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'pl_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['pla']['indexes_model'] = PlaSignalPassaggioPk



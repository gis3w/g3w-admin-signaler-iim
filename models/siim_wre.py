# coding=utf-8
"""" For Wrecks signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES


class RCombustibile(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    combustibile = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_combustibile'


class RDetFondale(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_det_fondale'


class RDisposizione(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_disposizione'


class RIdentificazione(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_identificazione'


class ROrientamento(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_orientamento'


class RPosizione(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_posizione'


class RScoperta(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    modalita_scoperta = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_scoperta'


class RSegnalamento(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_segnalamento'


class RSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField()
    id_regione = models.SmallIntegerField()
    id_prov = models.SmallIntegerField()
    id_comune = models.SmallIntegerField()
    località = models.CharField(max_length=255)
    tipo_seg = models.SmallIntegerField()
    tipo_rel = models.SmallIntegerField(blank=True, null=True)
    data_affon = models.DateField(blank=True, null=True)
    nome_bool = models.BooleanField()
    nome = models.CharField(max_length=255, blank=True, null=True)
    mater_bool = models.BooleanField()
    materiale = models.CharField(max_length=255, blank=True, null=True)
    lar_bool = models.BooleanField()
    larghezza = models.SmallIntegerField(blank=True, null=True)
    lun_bool = models.BooleanField()
    lunghezza = models.SmallIntegerField(blank=True, null=True)
    alt_bool = models.BooleanField()
    altezza = models.SmallIntegerField(blank=True, null=True)
    staz_bool = models.BooleanField()
    staz_disl = models.CharField(max_length=255, blank=True, null=True)
    car_bool = models.BooleanField()
    carico = models.CharField(max_length=255, blank=True, null=True)
    combust = models.SmallIntegerField()
    serb_bool = models.BooleanField()
    serbatoio = models.CharField(max_length=255, blank=True, null=True)
    nazione = models.CharField(max_length=255)
    comp_ascr = models.CharField(max_length=255)
    soc_arm = models.CharField(max_length=255)
    matricola = models.CharField(max_length=255)
    id_scop = models.SmallIntegerField()
    scop_carta = models.CharField(max_length=255, blank=True, null=True)
    rinvenim = models.SmallIntegerField()
    pos_relat = models.CharField(max_length=255, blank=True, null=True)
    coord_trnc = models.CharField(max_length=255, blank=True, null=True)
    punto_cosp = models.CharField(max_length=255, blank=True, null=True)
    det_posiz = models.SmallIntegerField()
    carta_ed = models.CharField(max_length=255, blank=True, null=True)
    fond_bool = models.BooleanField()
    fondale = models.SmallIntegerField(blank=True, null=True)
    batt_bool = models.BooleanField()
    battente = models.SmallIntegerField(blank=True, null=True)
    det_fond = models.SmallIntegerField()
    stato = models.SmallIntegerField()
    tronconi = models.SmallIntegerField(blank=True, null=True)
    disposiz = models.SmallIntegerField(blank=True, null=True)
    tipo_fond = models.SmallIntegerField()
    orientamen = models.SmallIntegerField(blank=True, null=True)
    storico = models.BooleanField(blank=True, null=True)
    ord_storic = models.CharField(max_length=255, blank=True, null=True)
    n_ord_stor = models.CharField(max_length=255, blank=True, null=True)
    seg_lum = models.BooleanField(blank=True, null=True)
    ordinanza = models.BooleanField()
    all_ord = models.CharField(max_length=255)
    nr_ord = models.CharField(max_length=255)
    foto = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['wre']['model'] = RSegnalazioni


class RSignalFeatures(models.Model):
    signal = models.ForeignKey(RSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'r_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['wre']['geo_model'] = RSignalFeatures



class RSignalVertex(models.Model):
    feature = models.ForeignKey(RSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['wre']['vx_model'] = RSignalVertex


class RStato(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_stato'


class RTipo(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    relitto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'r_tipo'



# ==============================================
# View to cumulate ids(pks)
# ==============================================
class WreSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'r_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['wre']['indexes_model'] = WreSignalPassaggioPk



# coding=utf-8
"""" For Draging and exavations signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES


class DAppaltatore(models.Model):
    id = models.IntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'd_appaltatore'


class DCompetenzads(models.Model):
    id = models.IntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'd_competenzaDS'


class DLivello(models.Model):
    id = models.IntegerField(primary_key=True)
    valore = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'd_livello'


class DSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField(blank=True, null=True)
    id_regione = models.SmallIntegerField(blank=True, null=True)
    id_prov = models.SmallIntegerField(blank=True, null=True)
    id_comune = models.SmallIntegerField(blank=True, null=True)
    comp_ds = models.CharField(max_length=255, blank=True, null=True)
    id_adsp = models.SmallIntegerField(blank=True, null=True)
    id_porto = models.SmallIntegerField(blank=True, null=True)
    concession = models.CharField(max_length=255, blank=True, null=True)
    localita = models.CharField(max_length=255, blank=True, null=True)
    tipo = models.SmallIntegerField(blank=True, null=True)
    anno_ds = models.IntegerField(blank=True, null=True)
    manutenz = models.SmallIntegerField(blank=True, null=True)
    profondita = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True)
    prof_norma = models.CharField(max_length=255, blank=True, null=True)
    livello = models.CharField(max_length=255, blank=True, null=True)
    prof_pescg = models.DecimalField(max_digits=7, decimal_places=3, blank=True, null=True)
    rilievo = models.SmallIntegerField(blank=True, null=True)
    ii_3176 = models.SmallIntegerField(blank=True, null=True)
    id_rilievo = models.CharField(max_length=255, blank=True, null=True)
    appaltator = models.CharField(max_length=255, blank=True, null=True)
    info_appal = models.CharField(max_length=255, blank=True, null=True)
    zona_reg = models.SmallIntegerField(blank=True, null=True)
    all_ord = models.CharField(max_length=255, blank=True, null=True)
    id_ord = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    chiusura = models.CharField(max_length=255, blank=True, null=True)
    archiviazione = models.CharField(max_length=255, blank=True, null=True)
    commenti = models.CharField(max_length=255, blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)



    class Meta:
        managed = False
        db_table = 'd_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['dae']['model'] = DSegnalazioni


class DSignalFeatures(models.Model):
    signal = models.ForeignKey(DSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPolygonField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'd_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['dae']['geo_model'] = DSignalFeatures


class DSignalVertex(models.Model):
    feature = models.ForeignKey(DSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'd_signal_vertex'

# Set for signal stype settings
SIIM_SIGNAL_TYPES['dae']['vx_model'] = DSignalVertex

# ==============================================
# View to cumulate ids(pks)
# ==============================================
class DaeSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'd_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['dae']['indexes_model'] = DaeSignalPassaggioPk



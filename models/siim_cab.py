# coding=utf-8
"""" For Cables signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES

        
class CvSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField()
    loc1 = models.CharField(max_length=255)
    loc2 = models.CharField(max_length=255)
    nome = models.CharField(max_length=255)
    id_tipo = models.SmallIntegerField()
    concession = models.CharField(max_length=255)
    pdc = models.CharField(max_length=255)
    tit_conc = models.CharField(max_length=255)
    connotaz = models.SmallIntegerField()
    anno_serv = models.DateField()
    stato = models.SmallIntegerField()
    verifica = models.DateField(blank=True, null=True)
    anod_catod = models.SmallIntegerField(blank=True, null=True)
    seg_lum = models.SmallIntegerField(blank=True, null=True)
    planimetr = models.SmallIntegerField(blank=True, null=True)
    allegati = models.CharField(max_length=255, blank=True, null=True)
    dati = models.SmallIntegerField(blank=True, null=True)
    ii_3176 = models.SmallIntegerField(blank=True, null=True)
    dati_share = models.SmallIntegerField(blank=True, null=True)
    sbp = models.CharField(max_length=255, blank=True, null=True)
    tre_d = models.SmallIntegerField(blank=True, null=True)
    sss = models.SmallIntegerField(blank=True, null=True)
    vf = models.SmallIntegerField(blank=True, null=True)
    zona_reg = models.SmallIntegerField(blank=True, null=True)
    all_ord = models.CharField(max_length=255, blank=True, null=True)
    id_ord = models.CharField(max_length=255, blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cv_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['cab']['model'] = CvSegnalazioni


class CvSignalFeatures(models.Model):
    signal = models.ForeignKey(CvSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.
    geom = models.MultiLineStringField(srid=3857, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cv_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['cab']['geo_model'] = CvSignalFeatures


class CvSignalVertex(models.Model):
    feature = models.ForeignKey(CvSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cv_signal_vertex'

# Set for signal stype settings
SIIM_SIGNAL_TYPES['cab']['vx_model'] = CvSignalVertex


class CvTipoCavi(models.Model):
    id = models.IntegerField(primary_key=True)
    tipo_cavo = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cv_tipo_cavi'




# ==============================================
# View to cumulate ids(pks)
# ==============================================
class CvSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'cv_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['obs']['indexes_model'] = CvSignalPassaggioPk



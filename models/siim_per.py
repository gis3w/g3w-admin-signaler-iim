# coding=utf-8
"""" For Platforms signal type Perimiters

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES


class PpPerimetroPoligonale(models.Model):
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    note = models.CharField(max_length=5, blank=True, null=True)
    tipo_scheda = models.SmallIntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pp_perimetro_poligonale'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['per']['model'] = PpPerimetroPoligonale


class PpSignalFeatures(models.Model):
    signal = models.ForeignKey(PpPerimetroPoligonale, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=400, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPolygonField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'pp_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['per']['geo_model'] = PpSignalFeatures


class PpSignalVertex(models.Model):
    feature = models.ForeignKey(PpSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=400, blank=True, null=True)
    caratteristiche = models.CharField(max_length=200, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pp_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['per']['vx_model'] = PpSignalVertex

# ==============================================
# View to cumulate ids(pks)
# ==============================================
class PpSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'pp_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['per']['indexes_model'] = PpSignalPassaggioPk



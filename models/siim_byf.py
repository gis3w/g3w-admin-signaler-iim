# coding=utf-8
"""" For Buoy fields signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.contrib.gis.db import models
from .admin import SIIM_SIGNAL_TYPES


class CbSegnalazioni(models.Model):
    tipo_scheda = models.SmallIntegerField()
    id_regione = models.SmallIntegerField()
    id_prov = models.SmallIntegerField()
    id_comune = models.SmallIntegerField()
    localita = models.CharField(max_length=255, blank=True, null=True)
    concession = models.CharField(max_length=255)
    pdc = models.CharField(max_length=255)
    tit_conc = models.CharField(max_length=255)
    nr_boe = models.SmallIntegerField()
    stagionale = models.BooleanField(blank=True, null=True)
    data_inst = models.DateField(blank=True, null=True)
    data_rimoz = models.DateField(blank=True, null=True)
    note_cb = models.CharField(max_length=255, blank=True, null=True)
    zona_reg = models.BooleanField(blank=True, null=True)
    all_ord = models.CharField(max_length=255, blank=True, null=True)
    id_ord = models.CharField(max_length=255, blank=True, null=True)
    note = models.CharField(max_length=255, blank=True, null=True)
    data = models.DateField(blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    passaggio = models.CharField(max_length=5, blank=True, null=True)
    creazione = models.DateTimeField(blank=True, null=True)
    aggiornamento = models.DateTimeField(blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.SmallIntegerField(blank=True, null=True)
    segn_pad_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cb_segnalazioni'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['byf']['model'] = CbSegnalazioni


class CbSignalFeatures(models.Model):
    signal = models.ForeignKey(CbSegnalazioni, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.MultiPolygonField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)
    shape = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'cb_signal_features'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['byf']['geo_model'] = CbSignalFeatures


class CbSignalVertex(models.Model):
    feature = models.ForeignKey(CbSignalFeatures, models.DO_NOTHING)
    nome = models.CharField(max_length=255, blank=True, null=True)
    gruppo = models.CharField(max_length=255, blank=True, null=True)
    commento = models.CharField(max_length=255, blank=True, null=True)
    caratteristiche = models.CharField(max_length=255, blank=True, null=True)
    geom = models.PointField(srid=3857, blank=True, null=True)
    user = models.IntegerField(blank=True, null=True)
    capit_porto = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cb_signal_vertex'


# Set for signal stype settings
SIIM_SIGNAL_TYPES['byf']['vx_model'] = CbSignalVertex



# ==============================================
# View to cumulate ids(pks)
# ==============================================
class ByfSignalPassaggioPk(models.Model):

    s_passaggio = models.CharField(max_length=5, blank=True, null=True)
    s_id = models.IntegerField(blank=True, null=True)
    sf_id = models.IntegerField(blank=True, null=True)
    sv_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'cb_signal_passaggio_pk'


# Set for signal type settings
SIIM_SIGNAL_TYPES['byf']['indexes_model'] = ByfSignalPassaggioPk



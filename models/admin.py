# coding=utf-8
""" Admin model for Signaler IIM module.

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-12-27'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'

from django.conf import settings
from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from qdjango.models import Project
from usersmanage.models import User

# Contains model names fro default db for routing.
TB_DEFAULT_ROUTE = []

SIIM_SIGNAL_TYPES = {
    'byf': {
        'desc': _('Buoy fields'),
        'geotype': 'MultiPolygon',
        'signaler_layer_origname': 'cb_segnalazioni',
        'geo_layer_origname': 'cb_signal_features',
        'vertex_layer_origname': 'cb_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note',

    },
    'plt': {
        'desc': _('Platforms'),
        'geotype': 'MultiPolygon',
        'signaler_layer_origname': 'pf_segnalazioni',
        'geo_layer_origname': 'pf_signal_features',
        'vertex_layer_origname': 'pf_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'cab': {
                'rules': [
                    {
                        'rule': {
                            'field': 'eletricita',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('cables, for mainland electrical connection'),
                            'show': False
                        }
                    }
                ]
            },
            'pip': {
                'rules': [
                    {
                        'rule': {
                            'field': 'coll_centr',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('pipelines, for connection to the control unit'),
                            'show': False
                        }
                    },
                    {
                        'rule': {
                            'field': 'coll_altre',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('pipelines, for connecting other platforms'),
                            'show': False
                        }
                    }
                ]
            },
            'obs': {
                'rules': [
                        {
                            'rule': {
                                'field': 'coll_centr',
                                'value': '1'
                            },
                            'msg': {
                                'label': _('obstacles, for anchoring to the bottom'),
                                'show': False
                            }
                        },
                    ]
            },
            'wel': {
                'rules': [
                    {
                        'rule': {
                            'field': 'pozzi',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('wells, for connected wells'),
                            'show': False
                        }
                    },
                ]
            },
            'sls': {
                'rules': [
                    {
                        'rule': {
                            'field': 'seg_lum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous signals'),
                            'show': False
                        }
                    },
                    {
                        'rule': {
                            'field': 'odas_off',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signals, for data acquisition station'),
                            'show': False
                        }
                    },
                    {
                        'rule': {
                            'field': 'boeproslum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous proximity buoys'),
                            'show': False
                        }
                    },
                    {
                        'rule': {
                            'field': 'foa',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for aerial obstacle lights'),
                            'show': False
                        }
                    }
                ]
            },
            'ppu': {
                'rules': [
                    {
                        'rule': {
                            'field': 'boeprosdiu',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('Perimeter'),
                            'show': True
                        }
                    },
                ]
            }
        },
        'note_field': 'note'

    },
    'wel': {
        'desc': _('Wells'),
        'geotype': 'MultiPoint',
        'signaler_layer_origname': 'p_segnalazioni',
        'geo_layer_origname': 'p_signal_features',
        'vertex_layer_origname': 'p_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'pip': {
                'rules': [
                    {
                        'rule': {
                            'field': 'collegam',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('pipelines, for connection to the control unit'),
                            'show': False
                        }
                    }
                ]
            }
        },
        'note_field': 'note'

    },
    'sls': {
        'desc': _('Light and sound signals'),
        'geotype': 'MultiPoint',
        'signaler_layer_origname': 's_segnalazioni',
        'geo_layer_origname': 's_signal_features',
        'vertex_layer_origname': 's_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note'

    },
    'cab': {
        'desc': _('Cables'),
        'geotype': 'MultiLineString',
        'signaler_layer_origname': 'cv_segnalazioni',
        'geo_layer_origname': 'cv_signal_features',
        'vertex_layer_origname': 'cv_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'sls': {
                'rules': [
                    {
                        'rule': {
                            'field': 'seg_lum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous signals'),
                            'show': False
                        }
                    }
                ]
            },
            'ppu': {
                'rules': [
                    {
                        'rule': {
                            'field': 'anod_catod',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('geometries relating to the presence of power lines'),
                            'show': True
                        }
                    },
                ]
            }
        },
        'note_field': 'note'

    },
    'pip': {
        'desc': _('Pipelines'),
        'geotype': 'MultiLineString',
        'signaler_layer_origname': 'c_segnalazioni',
        'geo_layer_origname': 'c_signal_features',
        'vertex_layer_origname': 'c_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'sls': {
                'rules': [
                    {
                        'rule': {
                            'field': 'seg_lum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous signals'),
                            'show': False
                        }
                    }
                ]
            },
            'ppl': {
                'rules': [
                    {
                        'rule': {
                            'field': 'diffusori',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('geometries relating to the diffusers'),
                            'show': True
                        }
                    },
                ]
            }
        },
        'note_field': 'note'

    },
    'obs': {
        'desc': _('Obstacles'),
        'geotype': 'MultiPoint',
        'signaler_layer_origname': 'o_segnalazioni',
        'geo_layer_origname': 'o_signal_features',
        'vertex_layer_origname': 'o_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'sls': {
                'rules': [
                    {
                        'rule': {
                            'field': 'seg_lum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous signals'),
                            'show': False
                        }
                    }
                ]
            },
            'per': {
                'rules': [
                    {
                        'rule': 'mandatory',
                        'msg': {
                            'label': _('geometry relative to the perimeter of the obstacle'),
                            'show': True
                        }
                    },
                ]
            }
        },
        'note_field': 'note'

    },
    'wre': {
        'desc': _('Wrecks'),
        'geotype': 'MultiPoint',
        'signaler_layer_origname': 'r_segnalazioni',
        'geo_layer_origname': 'r_signal_features',
        'vertex_layer_origname': 'r_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'sls': {
                'rules': [
                    {
                        'rule': {
                            'field': 'seg_lum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous signals'),
                            'show': False
                        }
                    }
                ]
            },
            'ppu': {
                'rules': [
                    {
                        'rule': {
                            'field': 'stato',
                            'value': '2'
                        },
                        'msg': {
                            'label': _('geometry relative to the position of stubs or debris'),
                            'show': True
                        }
                    },
                    {
                    'rule': {
                            'field': 'stato',
                            'value': '3'
                        },
                        'msg': {
                            'label': _('geometry relative to the position of stubs or debris'),
                            'show': True
                        }
                    },
                ]
            }
        },
        'note_field': 'note'

    },
    'fsa': {
        'desc': _('Marine farms'),
        'geotype': 'MultiPolygon',
        'signaler_layer_origname': 'zi_segnalazioni',
        'geo_layer_origname': 'zi_signal_features',
        'vertex_layer_origname': 'zi_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'sls': {
                'rules': [
                    {
                        'rule': {
                            'field': 'seg_lum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous signals'),
                            'show': False
                        }
                    }
                ]
            }
        },
        'note_field': 'note'

    },
    'pws': {
        'desc': _('Port works'),
        'geotype': 'MultiPolygon',
        'signaler_layer_origname': 'lp_segnalazioni',
        'geo_layer_origname': 'lp_signal_features',
        'vertex_layer_origname': 'lp_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'sls': {
                'rules': [
                    {
                        'rule': {
                            'field': 'seg_lum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous signals'),
                            'show': False
                        }
                    }
                ]
            }
        },
        'note_field': 'note'

    },
    'dae': {
        'desc': _('Dredging and excavations'),
        'geotype': 'MultiPolygon',
        'signaler_layer_origname': 'd_segnalazioni',
        'geo_layer_origname': 'd_signal_features',
        'vertex_layer_origname': 'd_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note'

    },
    'pla': {
        'desc': _('Planimetries'),
        'geotype': None,
        'signaler_layer_origname': 'pl_segnalazioni',
        'geo_layer_origname': None,
        'vertex_layer_origname': None,
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note'

    },
    'aeg': {
        'desc': _('Aereo generators'),
        'geotype': 'MultiPoint',
        'signaler_layer_origname': 'a_segnalazioni',
        'geo_layer_origname': 'a_signal_features',
        'vertex_layer_origname': 'a_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'cab': {
                'rules': [
                    {
                        'rule': 'mandatory',
                        'msg': {
                            'label': _('cables, for the route of the connection between wind turbines'),
                            'show': True
                        }
                    },
{
                        'rule': 'mandatory',
                        'msg': {
                            'label': _('cables, for connection to the grounded control unit'),
                            'show': True
                        }
                    },
                    {
                        'rule': {
                            'field': 'trasformaz',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('cables, for connection with Central to the sea'),
                            'show': False
                        }
                    }
                ]
            },
            'obs': {
                'rules': [
                    {
                        'rule': {
                            'field': 'ancoraggi',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('obstacles, for anchoring to the bottom'),
                            'show': False
                        }
                    }
                ]
            },
            'sls': {
                'rules': [
                    {
                        'rule': {
                            'field': 'seg_lum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous signals'),
                            'show': False
                        }
                    },
                    {
                        'rule': {
                            'field': 'boa_odas',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signals, for data acquisition station'),
                            'show': False
                        }
                    },
                    {
                        'rule': {
                            'field': 'boeproslum',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for luminous proximity buoys'),
                            'show': False
                        }
                    },
{
                        'rule': {
                            'field': 'foa',
                            'value': '1'
                        },
                        'msg': {
                            'label': _('signaling, for aerial obstacle lights'),
                            'show': False
                        }
                    }
                ]
            },
            'per': {
                'rules': [
                    {
                        'rule': 'mandatory',
                        'msg': {
                            'label': _('geometry relating to the wind farm'),
                            'show': True
                        }
                    },
                ]
            }
        },
        'note_field': 'note'

    },
    'iin': {
        'desc': _('Navigational warning'),
        'geotype': 'MultiPoint',
        'signaler_layer_origname': 'in_segnalazioni',
        'geo_layer_origname': 'in_signal_features',
        'vertex_layer_origname': 'in_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {
            'per': {
                'rules': [
                    {
                        'rule': 'mandatory',
                        'msg': {
                            'label': _('Perimeter'),
                            'show': True
                        }
                    },
                ]
            }
        },
        'note_field': 'note'

    },
    'per': {
        'desc': _('Perimeter'),
        'geotype': 'MultiPolygon',
        'signaler_layer_origname': 'pp_perimetro_poligonale',
        'geo_layer_origname': 'pp_signal_features',
        'vertex_layer_origname': 'pp_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note'

    },
    'ppl': {
        'desc': _('Linear Perimeter'),
        'geotype': 'MultiLineString',
        'signaler_layer_origname': 'ppl_perimetro_lineare',
        'geo_layer_origname': 'ppl_signal_features',
        'vertex_layer_origname': 'ppl_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note'

    },
    'ppu': {
        'desc': _('Punctual Perimeter'),
        'geotype': 'MultiPoint',
        'signaler_layer_origname': 'ppu_perimetro_puntiforme',
        'geo_layer_origname': 'ppu_signal_features',
        'vertex_layer_origname': 'ppu_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note'

    },
    'tes': {
        'desc': _('TEST'),
        'geotype': 'MultiPolygon',
        'signaler_layer_origname': 'sls_segnalazioni',
        'geo_layer_origname': 'sls_signal_features',
        'vertex_layer_origname': 'sls_signal_vertex',
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note'

    },
    'ril': {
        'desc': _('Surveys'),
        'geotype': None,
        'signaler_layer_origname': 'ril_segnalazioni',
        'geo_layer_origname': None,
        'vertex_layer_origname': None,
        'state_layer_origname': 'stati',
        'ab_signal_fields': [
            'id',
            'tipo_segn',
            'passaggio'
        ],
        'user_field': 'user',
        'cp_field': 'capit_porto',
        'father_field': 'segn_pad_id',
        'relation_signal_types': {},
        'note_field': 'note'

    }
}

TYPES = []
for t, d in SIIM_SIGNAL_TYPES.items():
    TYPES.append((t, d['desc']))


class SIIMSignalerProjects(models.Model):
    """ Model for set projects for every signaler type"""

    type = models.CharField(_('Signaler type'), max_length=3, choices=TYPES)
    lang = models.CharField(_('Language'), max_length=3, choices=settings.LANGUAGES, default='it')
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    note = models.TextField(_('Description'), null=True, blank=True)

    class Meta:
        permissions = (
            ('make_report', 'Can make Signaler IIM report'),
            ('manage_reports', 'Can manage Signaler IIM reports'),
            ('create_siim_users', 'Can create Signaler IIM users'),
            ('update_siim_users', 'Can update Signaler IIM users'),
            ('delete_siim_users', 'Can view Signaler IIM users'),
            ('view_siim_users', 'Can view Signaler IIM users')

        )

    @classmethod
    def get_project(cls, type: str, lang='it'):
        """ Get qdjango Project Instance if exists by 'type' """

        try:
            return cls.objects.get(type=type, lang=lang).project
        except:
            if lang != 'it':
                # Try again with default 'it' project
                try:
                    return cls.objects.get(type=type, lang='it').project
                except:
                    return None
            return None

    @classmethod
    def get_type(cls, prj: Project):
        """ Get signal type by qdjango project instance """

        try:
            return cls.objects.get(project=prj).type
        except:
            return None

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)

        # check for unique project for Signaler IIM by signal type and language only for new
        try:
            siimproject = SIIMSignalerProjects.objects.get(type=self.type, lang=self.lang)
        except:
            siimproject = None

        if siimproject and not self.pk:
            raise ValidationError(
                {'project': _(f"A project is just set for signal type {SIIM_SIGNAL_TYPES[self.type]['desc']} "
                              f"and language {self.lang}")})


# For db routing
TB_DEFAULT_ROUTE.append(SIIMSignalerProjects._meta.model_name)


IIM_DEPARTMENTS = [
    [None, '------'],
    ['ces', 'COORDINAMENTO E STANDARDIZZAZIONE'],
    ['sge', 'SUPPORTO GEOSPAZIALE'],
    ['idr', 'IDROGRAFIA']
]


class SIIMUserExtraData(models.Model):
    """ Model to save extra user data, i.e. the captaincy (cp) of belonging """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(_('Phone number'), max_length=20,  null=True, blank=True)
    cp = models.IntegerField(_('Port captaincy'), null=True, blank=True)
    iim_office = models.CharField(_('Office'), max_length=5, choices=IIM_DEPARTMENTS, blank=True, null=True)

    @property
    def cp_name(self):

        try:
            from signaler_iim.models import SlsbCp
            return SlsbCp.objects.get(pk=self.cp)
        except:
            return None

    @property
    def iim_office_name(self):

        offices = {o[0]:o[1] for o in IIM_DEPARTMENTS}
        try:
            return offices[self.iim_office]
        except:
            return None



# For db routing
TB_DEFAULT_ROUTE.append(SIIMUserExtraData._meta.model_name)


class SIIMSignalNote(TimeStampedModel):
    """ Model to note for every signal by user datetime and signal type """

    type = models.CharField(_('Signaler type'), max_length=3, choices=TYPES)
    signal_id = models.IntegerField(_('Signal ID'))
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    from_state = models.CharField(_('From state'), max_length=20, null=True, blank=True)
    to_state = models.CharField(_('To state'), max_length=20, null=True, blank=True)
    note = models.TextField(_('Signal note'))




# For db routing
TB_DEFAULT_ROUTE.append(SIIMSignalNote._meta.model_name)





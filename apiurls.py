# coding=utf-8
""""
    API urls for Signaler IIM module.
.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-12-30'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'


from django.urls import re_path, path
from django.contrib.auth.decorators import login_required
from .api.views import SIIMLayerVectorView, SIIMSignalNoteView, SIIMSignalGetAncestor

BASE_URLS = 'vector_iim'

urlpatterns = [
    re_path(r'^api/(?P<mode_call>data|config|shp|xls|gpkg|gpx|csv|filtertoken)/(?P<project_type>[-_\w\d]+)/(?P<project_id>[0-9]+)/'
        r'(?P<layer_name>[-_\w\d]+)/$',
        SIIMLayerVectorView.as_view(), name='signaler-iim-vector-api'),
    path('api/note/list/<str:signal_type>/<int:signal_id>/', login_required(SIIMSignalNoteView.as_view()),
         name='signaler-iim-note-list'),
    path('api/getancestor/<str:signal_type>/<int:signal_id>/', login_required(SIIMSignalGetAncestor.as_view()),
         name='signaler-iim-getancestor')
]
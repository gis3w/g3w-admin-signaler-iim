# coding=utf-8
""""
    REST vector APi views for Signaler IMM module.
.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-12-30'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'

from qdjango.vector import LayerVectorView
from core.signals import post_create_maplayerattributes
from core.utils.structure import APIVectorLayerStructure, mapLayerAttributesFromQgisLayer
#from django.views.decorators.csrf import csrf_exempt
from signaler_iim.utils import get_signal_type
from signaler_iim.api.filters import \
    StatiFilterBackend, \
    SignalByUserFilterBackend


class SIIMLayerVectorView(LayerVectorView):

    filter_backends = LayerVectorView.filter_backends + (
        StatiFilterBackend,
        SignalByUserFilterBackend
    )

    def dispatch(self, request, *args, **kwargs):

        # set with siim signal type
        self.signal_type = get_signal_type(kwargs['project_id'])
        return super().dispatch(request, *args, **kwargs)

    def set_metadata_layer(self, request, **kwargs):
        super().set_metadata_layer(request, **kwargs)

    def response_data_mode(self, request, export_features=False):
        # Switch for layers custom
        super().response_data_mode(request, export_features)

    def response_config_mode(self, request):
        """
        Perform config operation, return form fields data for editing layer.
        :param request: API Object Request
        :param layer_name: editing layer name
        :return: Vector params
        """

        forms = self.get_forms()

        # add forms data if exist
        kwargs = {'fields': forms[self.layer_name]['fields']
                  } if forms and forms.get(self.layer_name) else {}

        if hasattr(self.metadata_layer, 'fields_to_exclude'):
            kwargs['exclude'] = self.metadata_layer.fields_to_exclude
        if hasattr(self.metadata_layer, 'order'):
            kwargs['order'] = self.metadata_layer.order

       # FIXME: is this a wreck of pre-qdjango era? Can we remove it and always use mapLayerAttributesFromQgisLayer ?
        if self.mapping_layer_attributes_function.__func__ == mapLayerAttributesFromQgisLayer:
            fields = list(self.mapping_layer_attributes_function.__func__(
                self.metadata_layer.qgis_layer,
                **kwargs
            ).values())
        else:
            fields = list(self.mapping_layer_attributes_function.__func__(
                self.layer,
                formField=True,
                **kwargs
            ).values())

        vector_params = {
            'geometryType': self.metadata_layer.geometry_type,
            'fields': fields,
        }

        # post_create_maplayerattributes signal
        post_create_maplayerattributes.send(
            self, layer=self.layer, vector_params=vector_params, user=request.user)

        self.results.update(APIVectorLayerStructure(**vector_params).as_dict())




# @csrf_exempt  # put exempt here because as_view method is outside url bootstrap declaration
# def siim_layer_vector_view(request, project_type, project_id, layer_name, *args, **kwargs):
#
#     # instance module vector view
#     view = SIIMLayerVectorView.as_view()
#     kwargs.update({'project_type': project_type, 'project_id': project_id, 'layer_name': layer_name})
#     return view(request, *args, **kwargs)

# coding=utf-8
"""" Django Views for admin API signal management:

Code derived from https://appliku.com/post/django-rest-framework-and-datatable-example

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""


__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-05'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'


from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from usersmanage.utils import userHasGroups
from core.api.base.views import G3WAPIView
from signaler_iim.api.serializers import SIIM_SIGNAL_TYPES, SIIMSignalNoteSerializer
from signaler_iim.api.permisions import ProjectRelationPermission
from signaler_iim.utils import get_signals_by_user, get_acenstor
from signaler_iim.models import SIIMSignalNote, Stati

from signaler_iim.api.filters.signal import DTableFilter

import copy



class DTSignalsAPIView(ListAPIView):
    """ List API view of signals models for DataTable"""

    permission_classes = [
        ProjectRelationPermission
    ]

    signal_type = None

    def get_queryset(self):
        return get_signals_by_user(self.signal_type, self.request.user).order_by('-creazione')

    def get_serializer_class(self):
        """ Get serializer class by SIIM signal type """
        return SIIM_SIGNAL_TYPES[self.signal_type]['serializer']

    def filter_for_datatable(self, queryset):

        serializer = self.get_serializer_class()
        fields = copy.copy(serializer.Meta.fields)

        # Filtering
        # -----------------------------
        filter = DTableFilter.factory(self.signal_type, fields, self.request)
        queryset = queryset.filter(**filter.ksearchargs)

        # Ordering
        # -----------------------------
        ordering_column = self.request.query_params.get('order[0][column]')
        ordering_direction = self.request.query_params.get('order[0][dir]')
        ordering = fields[int(ordering_column)]

        if ordering and ordering_direction == 'desc':
            ordering = f"-{ordering}"
        if ordering:
            queryset = queryset.order_by(ordering)
        return queryset

    def list(self, request, *args, **kwargs):

        # Set signal type
        self.signal_type = kwargs['type']

        draw = request.query_params.get('draw')
        queryset = self.filter_queryset(self.get_queryset())
        records_total = queryset.count()
        filtered_queryset = self.filter_for_datatable(queryset)

        try:
            start = int(request.query_params.get('start'))
        except ValueError:
            start = 0
        except TypeError:
            start = 0

        try:
            length = int(request.query_params.get('length'))
        except ValueError:
            length = 10
        except TypeError:
            length = 10

        end = length + start
        serializer = self.get_serializer(filtered_queryset[start:end], many=True, request=self.request)
        response = {
            'draw': draw,
            'recordsTotal': records_total,
            'recordsFiltered': filtered_queryset.count(),
            'data': serializer.data
        }
        return Response(response)


class SIIMSignalNoteView(ListAPIView):
    """ Return list note by signal_type and signal_id """

    serializer_class = SIIMSignalNoteSerializer

    def get_queryset(self):
        return SIIMSignalNote.objects.filter(type=self.kwargs['signal_type'], signal_id=self.kwargs['signal_id'])


class SIIMSignalGetAncestor(G3WAPIView):
    """ Give a signal_id and a signal_type return ancestor """

    def get(self, request, **kwargs):

        ancestor, type = get_acenstor(f"{kwargs['signal_id']}:{kwargs['signal_type']}")

        data = SIIM_SIGNAL_TYPES[type]['serializer'](ancestor).data
        data['passaggio'] = Stati.get_value(data['passaggio'])

        self.results.results.update({
            "data": data
        })

        return Response(self.results.results)







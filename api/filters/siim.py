# coding=utf-8
"""" Signaler IIM vector filters

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-10'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'


from django.conf import settings
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsCoordinateTransform,
    QgsFeatureRequest,
    QgsRectangle,
    QgsCoordinateTransformContext,
)
from rest_framework.exceptions import ParseError
from usersmanage.utils import userHasGroups
from core.api.filters import BaseFilterBackend
from signaler_iim.models import SIIM_SIGNAL_TYPES, SIIMSignalerProjects, SlsbCp
from signaler_iim.utils import get_layer_orignames


import logging

logger = logging.getLogger('signaler_iim')


class StatiFilterBackend(BaseFilterBackend):
    """Backend filter for Stati layer"""

    def apply_filter(self, request, metadata_layer, qgis_feature_request, view):


        # First off all check for layer stati
        if metadata_layer.qgis_layer.name() == 'stati':

            # Try to get signal_type by project layer
            signal_type = SIIMSignalerProjects.get_type(view.layer.project)
            signal_id = request.query_params.get('signal_id')
            current_user = request.user

            # Only for SIIM_USERS_GROUP users:
            if userHasGroups(request.user, settings.SIIM_USERS_GROUP.values()):

                try:
                    signal = SIIM_SIGNAL_TYPES[signal_type]['model'].objects.get(pk=signal_id)
                except Exception as e:
                    logger.warning(f'[SIGNALER IIM]: StatiFilterBackend - {e}')
                    signal = None
                    signal_param_exist = signal_id == '_new_'

                # Make an expression statement based on current_user and current signal state
                # ---------------------------------------------------------------------------
                #expression = "\"value\" IN ('XXXXXXX', )"
                expression = ""

                if signal:

                    if userHasGroups(current_user, [settings.SIIM_USERS_GROUP['ccp_reporter']]):

                        # For CCP REPORTER:
                        #-----------------
                        if signal.passaggio in ('1IL', ):
                            expression = "\"value\" IN ('1IL', '2DV')"
                        elif signal.passaggio in ('3RINC', '3RICO'):
                            expression = "\"value\" IN ('1IL')"
                        else:
                            expression = "\"value\" IN ('XXXXXXX', )"

                    elif userHasGroups(current_user, [settings.SIIM_USERS_GROUP['ccp_supervisor']]):

                        # For CCP SUPERVISOR:
                        # -----------------
                        if signal.passaggio in ('2DV',):
                            expression = "\"value\" IN ('1IL', '2DV', '2PU', '2PR')"
                        else:
                            expression = "\"value\" IN ('XXXXXXX', )"
                    elif userHasGroups(current_user, [settings.SIIM_USERS_GROUP['iim_examiner']]):

                        # For IIM EXAMINER:
                        # -----------------
                        if signal.passaggio in ('2PU', '2PR'):
                            expression = "\"value\" IN ('3IE')"
                        else:
                            expression = "\"value\" IN ('XXXXXXX', )"

                    elif userHasGroups(current_user, [settings.SIIM_USERS_GROUP['iim_responsible']]):

                        # For IIM RESPONSIBLE:
                        # --------------------
                        if signal.passaggio in ('3IE',):
                            expression = "\"value\" IN ('3RINC', '3RICO')"
                        elif signal.passaggio in ('3RICO',):
                            expression = "\"value\" IN ('4C', '4A')"
                        else:
                            expression = "\"value\" IN ('XXXXXXX', )"

                else:

                    # New signal
                    # ============================
                    if userHasGroups(current_user, [settings.SIIM_USERS_GROUP['ccp_reporter']]) and signal_param_exist:
                        expression = "\"value\" IN ('1IL', '2DV')"
                    elif userHasGroups(current_user, [settings.SIIM_USERS_GROUP['ccp_supervisor']]) and signal_param_exist:
                        expression = "\"value\" IN ('1IL', '2DV', '2PU', '2PR')"


                if expression:

                    current_expression = qgis_feature_request.filterExpression()

                    if current_expression:
                        search_expression = '( %s ) AND ( %s )' % (
                            current_expression.dump(), expression)

                        qgis_feature_request.setFilterExpression(search_expression)

                    else:
                        qgis_feature_request.setFilterExpression(expression)


class SignalByUserFilterBackend(BaseFilterBackend):
    """Backend filter for Filter signal layers by user and Cp"""

    def apply_filter(self, request, metadata_layer,  qgis_feature_request, view):

        # Try to get signal_type by project layer
        signal_type = SIIMSignalerProjects.get_type(view.layer.project)
        current_user = request.user

        layers = get_layer_orignames(signal_type, view.layer.project, '_')

        if metadata_layer.qgis_layer.id() in layers.values():

            expression = None

            # First condition user must become one of SIIM-CCP-REPORTERS and is set a signal_type
            if userHasGroups(current_user,
                             [settings.SIIM_USERS_GROUP['ccp_reporter'], settings.SIIM_USERS_GROUP['ccp_supervisor']]):

                if signal_type not in SIIM_SIGNAL_TYPES.keys():

                    # To avoid show every signal data
                    qgis_feature_request.setFilterExpression(f"\"user\" = -999999")
                else:
                    #expression = f"\"user\" = {request.user.pk}"

                    # For user with CP set
                    if hasattr(request.user, 'siimuserextradata'):

                        # if SIIM_SIGNAL_TYPES[signal_type]['signaler_layer_origname'] == view.layer.origname:
                        expression = f"\"capit_porto\" = '{request.user.siimuserextradata.cp}'"
                        # else:
                        #
                        #     # Get signal id with same CP of differente type form signal_type
                        #     model = SIIM_SIGNAL_TYPES[signal_type]['model']
                        #     signals = "'" + "','".join([str(s.pk) for s in model.objects.filter(
                        #         capit_porto=request.user.siimuserextradata.cp)]) + "'"
                        #     if signals:
                        #         expression = f"\"signal_id\" IN ({signals})"

                    else:

                        # For users without siimextradata, no filter.
                        return

            elif userHasGroups(current_user,
                               [settings.SIIM_USERS_GROUP['iim_responsible'], settings.SIIM_USERS_GROUP['iim_examiner']]):

                # Filter signals not in state 'in lavorazione'
                if signal_type not in SIIM_SIGNAL_TYPES.keys():
                    # To avoid show every signal data
                    qgis_feature_request.setFilterExpression(f"\"user\" = -999999")
                else:

                    indexes_model = SIIM_SIGNAL_TYPES[signal_type]['indexes_model']
                    idxes = indexes_model.objects.filter(s_passaggio__in=['1IL', '2DV'])

                    signals = None
                    if SIIM_SIGNAL_TYPES[signal_type]['signaler_layer_origname'] == view.layer.origname:
                        idxes = idxes.distinct('s_id')
                        signals = ",".join([str(i.s_id) for i in idxes])
                    elif SIIM_SIGNAL_TYPES[signal_type]['geo_layer_origname'] == view.layer.origname:
                        idxes = idxes.distinct('sf_id')
                        signals = ",".join([str(i.sf_id) for i in idxes if i.sf_id])
                    elif SIIM_SIGNAL_TYPES[signal_type]['vertex_layer_origname'] == view.layer.origname:
                        idxes = idxes.distinct('sv_id')
                        signals = ",".join([str(i.sv_id) for i in idxes if i.sv_id])

                    if signals:
                        expression = f"\"id\" NOT IN ({signals})"



            if expression:
                current_expression = qgis_feature_request.filterExpression()

                if current_expression:
                    search_expression = '( %s ) AND ( %s )' % (
                        current_expression.dump(), expression)

                    qgis_feature_request.setFilterExpression(search_expression)

                else:
                    qgis_feature_request.setFilterExpression(expression)



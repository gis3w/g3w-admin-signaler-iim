# coding=utf-8
""" API permission classes for signal

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-19'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from rest_framework.permissions import BasePermission


class ProjectRelationPermission(BasePermission):
    """
    Allows access only to users have permission manage_reports
    """

    def has_permission(self, request, view):
        return request.user.has_perm('signaler_iim.manage_reports')

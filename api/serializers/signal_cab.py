# coding=utf-8
"""" For Cables signal type

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-17'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'


from rest_framework.serializers import ModelSerializer, SerializerMethodField
from signaler_iim.api.serializers import SIIMSignalsReceiverMixin
from signaler_iim.models import \
    SIIM_SIGNAL_TYPES, \
    CvSegnalazioni


class CabSegnalazioniSerializer(SIIMSignalsReceiverMixin, ModelSerializer):

    passaggio = SerializerMethodField()
    segn_pad_id = SerializerMethodField()
    tipo_scheda = SerializerMethodField()
    capit_porto = SerializerMethodField()
    user = SerializerMethodField()
    map_link = SerializerMethodField()

    # Signal type
    type = 'cab'


    class Meta:
        model = CvSegnalazioni
        fields = [
            'id',
            'passaggio',
            'segn_pad_id',
            'tipo_scheda',
            'capit_porto',
            'user',
            'creazione',
            'aggiornamento',
            'map_link'
        ]


SIIM_SIGNAL_TYPES['cab']['serializer'] = CabSegnalazioniSerializer
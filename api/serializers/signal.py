# coding=utf-8
"""" Admin signal API serializers

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-05'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.urls import reverse
from django.utils.translation import get_language
from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest_framework.fields import empty
from usersmanage.models import User
from signaler_iim.models import \
    SIIM_SIGNAL_TYPES, \
    SlsSegnalazioni, \
    SSegnalazioni, \
    Stati, \
    SlsbTipoScheda, \
    SIIMSignalerProjects, \
    SlsbSeglum, \
    SlsbCp, \
    SIIMSignalNote
from signaler_iim.utils import get_layer_orignames, get_map_link as map_link, get_role, get_acenstor


class SIIMSignalNoteSerializer(ModelSerializer):
    """ Serializer for note of a signal"""

    from_state = SerializerMethodField()
    to_state = SerializerMethodField()
    user = SerializerMethodField()

    def get_from_state(self, obj):
        try:
            return Stati.objects.get(value=obj.from_state).desc
        except:
            return None

    def get_to_state(self, obj):
        try:
            return Stati.objects.get(value=obj.to_state).desc
        except:
            return None

    def get_user(self, obj):
        try:
            return f"{obj.user.username} [{get_role(obj.user)}]"
        except:
            return None

    class Meta:
        model = SIIMSignalNote
        fields = '__all__'


class SIIMSignalsReceiverMixin(object):
    """ Mixin for Signals receivers """

    def get_user(self, obj):
        try:
            return User.objects.get(id=obj.user).username
        except:
            return None

    def get_passaggio(self, obj):
        try:
            if obj.segn_pad_id:
                m, t = get_acenstor(obj.segn_pad_id)
                return Stati.objects.get(value=m.passaggio).desc
            else:
                return Stati.objects.get(value=obj.passaggio).desc
        except:
            return None

    def get_capit_porto(self, obj):
        try:
            return SlsbCp.objects.get(id=obj.capit_porto).id_loc
        except:
            return None

    def get_map_link(self, obj):
        """ Return a link to map to zoom to signal """

        return map_link(self.type, obj.pk, lang=get_language())

    def get_tipo_scheda(self, obj: SlsSegnalazioni):
        try:
            return SlsbTipoScheda.objects.get(id=obj.tipo_scheda).tipo_scheda
        except:
            return None

    def get_segn_pad_id(self, obj):
        """ Return a link to map to zoom to father signal """

        if obj.segn_pad_id:

            # Get ancestor
            m, t = get_acenstor(obj.segn_pad_id)
            return map_link(t, m.pk)
        else:
            return None

    def __init__(self, instance=None, data=empty, **kwargs):

        if 'request' in kwargs:
            self.request = kwargs['request']
            del (kwargs['request'])

        super().__init__(instance=instance, data=data, **kwargs)




class SlsSegnalazioniSerializer(SIIMSignalsReceiverMixin, ModelSerializer):

    passaggio = SerializerMethodField()
    segn_pad_id = SerializerMethodField()
    tipo_scheda = SerializerMethodField()
    capit_porto = SerializerMethodField()
    user = SerializerMethodField()
    map_link = SerializerMethodField()

    # Signal type
    type = 'tes'

    class Meta:
        model = SlsSegnalazioni
        fields = [
            'id',
            'segn_pad_id',
            'passaggio',
            'tipo_scheda',
            'capit_porto',
            'user',
            'creazione',
            'aggiornamento',
            'map_link'
        ]


SIIM_SIGNAL_TYPES['tes']['serializer'] = SlsSegnalazioniSerializer

class SSegnalazioniSerializer(SIIMSignalsReceiverMixin, ModelSerializer):

    passaggio = SerializerMethodField()
    segn_pad_id = SerializerMethodField()
    tipo_scheda = SerializerMethodField()
    capit_porto = SerializerMethodField()
    user = SerializerMethodField()
    map_link = SerializerMethodField()

    # Signal type
    type = 'sls'

    class Meta:
        model = SSegnalazioni
        fields = [
            'id',
            'segn_pad_id',
            'passaggio',
            'tipo_scheda',
            'capit_porto',
            'user',
            'creazione',
            'aggiornamento',
            'map_link'
        ]


SIIM_SIGNAL_TYPES['sls']['serializer'] = SSegnalazioniSerializer

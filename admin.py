from django.contrib import admin
from .models import SIIMSignalNote


@admin.register(SIIMSignalNote)
class SIIMSignalNoteAdmin(admin.ModelAdmin):
    list_display = (
        'created',
        'user',
        'from_state',
        'to_state'
    )

# Plugins module for Signaler IIM project



### SETTINGS
Add database settings for DB data:

```python
DATABASES = {
    ...
    'signaler_iim': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': '<sgnaler_iim_db_data>',
        'USER': '<sgnaler_iim_db_username>',
        'PASSWORD': '<sgnaler_iim_db_password>',
        'HOST': '<sgnaler_iim_db_host>',
        'PORT': '<sgnaler_iim_db_port>'
    },
    ...
}
```

Add database routers:

```python
DATABASE_ROUTERS = [
    ...
    'signaler_iim.db.router.SIIMRouter',
]
```

## FOR TESTING

Remember to set settings `TEST_RUNNER` to `'signaler_iim.tests.testrunner.SIIMDiscoverRunner'` 
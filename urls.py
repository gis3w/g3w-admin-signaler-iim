# coding=utf-8
""""
    Urls module for Signaler IIM module.
.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-12-27'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'

from django.urls import path, re_path
from django.contrib.auth.decorators import login_required
from base.urls import G3W_SITETREE_I18N_ALIAS
from signaler_iim.views import \
    SIIMProjectsListView, \
    SIIMProjectAddView, \
    SIIMProjectUpdateView, \
    SIIMProjectDeleteView, \
    SIIMSignalsView, \
    SIIMSignalsListView, \
    SIIMSignalsDownloadImgView, \
    SIIMSignalsDownloadZipView, \
    SIIMUserCreateView, \
    SIIMUserListView, \
    SIIMUserUpdateView, \
    SIIMUserAjaxDeleteView

from .api.views import \
    DTSignalsAPIView

from signaler_iim.models import SIIM_SIGNAL_TYPES

# For sitetree bar translation
G3W_SITETREE_I18N_ALIAS.append('signaler_iim')


urlpatterns = [

    # For projects
    # ------------
    path('projects/', login_required(SIIMProjectsListView.as_view()), name='siim-project-list'),
    path('projects/add/', login_required(SIIMProjectAddView.as_view()), name='siim-project-add'),
    path('projects/update/<int:pk>/', login_required(SIIMProjectUpdateView.as_view()),
         name='siim-project-update'),
    path('projects/delete/<int:pk>/', login_required(SIIMProjectDeleteView.as_view()),
         name='siim-project-delete'),

    # For signals management
    # ----------------------
    path('signals/', login_required(SIIMSignalsView.as_view()), name='ssim-signals-dashboard'),
    re_path(r'signals/(?P<type>{})/$'.format('|'.join(SIIM_SIGNAL_TYPES.keys())),
            login_required(SIIMSignalsListView.as_view()), name='ssim-signals-list-by-type'),

    # For datatable ajax call
    # -----------------------
    re_path(r'signals/dtdata/(?P<type>{})/$'.format('|'.join(SIIM_SIGNAL_TYPES.keys())),
            DTSignalsAPIView.as_view(), name='ssim-signals-api-list-by-type'),

    # For download/export signal sheet
    re_path(r'signals/png/(?P<type>{})/(?P<pk>[0-9]+)/$'.format('|'.join(SIIM_SIGNAL_TYPES.keys())),
            login_required(SIIMSignalsDownloadImgView.as_view()), name='siim-signals-download-img-pk'),
    re_path(r'signals/png/(?P<type>{})/$'.format('|'.join(SIIM_SIGNAL_TYPES.keys())),
            login_required(SIIMSignalsDownloadImgView.as_view()), name='siim-signals-download-img'),
    re_path(r'signals/(?P<mode_export>zip|s57)/(?P<type>{})/(?P<pk>[0-9]+)/$'.format('|'.join(SIIM_SIGNAL_TYPES.keys())),
            login_required(SIIMSignalsDownloadZipView.as_view()), name='siim-signals-download-zip-pk'),
    re_path(r'signals/(?P<mode_export>zip|s57)/(?P<type>{})/$'.format('|'.join(SIIM_SIGNAL_TYPES.keys())),
            login_required(SIIMSignalsDownloadZipView.as_view()), name='siim-signals-download-zip'),

    # Users management
    # ----------------
    re_path(r'^users/$', login_required(SIIMUserListView.as_view()), name='siim-user-list'),
    re_path(r'^users/add/$', login_required(SIIMUserCreateView.as_view()), name='siim-user-add'),
    re_path(r'^users/update/(?P<pk>[-_\w\d]+)/$', login_required(SIIMUserUpdateView.as_view()),
            name='siim-user-update'),
    re_path(r'^users/delete/(?P<pk>[-_\w\d]+)/$',login_required(SIIMUserAjaxDeleteView.as_view()),
            name='siim-user-delete'),




]
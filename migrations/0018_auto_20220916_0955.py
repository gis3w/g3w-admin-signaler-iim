# Generated by Django 2.2.28 on 2022-09-16 09:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('signaler_iim', '0017_auto_20220916_0644'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siimsignalerprojects',
            name='lang',
            field=models.CharField(choices=[('it', 'Italian'), ('en', 'English'), ('fr', 'French'), ('fi', 'Finnish'), ('se', 'Swedish'), ('ro', 'Romanian'), ('de', 'Deutsch')], default='it', max_length=3, verbose_name='Language'),
        ),
    ]

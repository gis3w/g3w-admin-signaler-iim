# coding=utf-8
"""" Template tags for Signaler IIM module

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-11'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'


from django import template
from signaler_iim.models import SIIM_SIGNAL_TYPES

register = template.Library()


@register.filter
def signal_type(type):
    """
    Template filter to get value from dict by key
    """
    return SIIM_SIGNAL_TYPES[type]['desc']
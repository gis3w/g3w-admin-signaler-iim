# coding=utf-8
"""" Signals management for Signaler IIM module.

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-03'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.conf import settings
from django.utils.translation import get_language
from django.http.response import HttpResponse, Http404
from django.views.generic import TemplateView, View
from django_weasyprint.views import WeasyTemplateView, CONTENT_TYPE_PNG
from django.utils.decorators import method_decorator
from guardian.decorators import permission_required
from core.utils.qgisapi import get_qgis_layer
from qdjango.utils.urls import get_map_url
from signaler_iim.utils import dec2dd, get_signals_by_user, get_layer_orignames
from signaler_iim.models import SIIM_SIGNAL_TYPES, Stati, SIIMSignalerProjects

from qgis.core import \
    QgsFields, \
    QgsField, \
    QgsVectorFileWriter, \
    QgsWkbTypes, \
    QgsCoordinateReferenceSystem, \
    QgsFeature, \
    QgsGeometry, \
    QgsPointXY,\
    QgsCoordinateTransformContext, \
    QgsFeatureRequest, \
    QgsVectorLayer


from qgis.PyQt.QtCore import QVariant, NULL

from copy import copy
from io import BytesIO
import json
import os
import tempfile
import zipfile
import py7zr
import csv

import logging

logger = logging.getLogger('signaler_iim')



class SIIMSignalsView(TemplateView):
    """ View for show Signals dashboard """

    template_name = 'signaler_iim/signals_dashboard.html'

    @method_decorator(permission_required('signaler_iim.manage_reports', return_403=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):

        cx = super().get_context_data(**kwargs)

        # Add signals type
        cx['signals_type'] = copy(SIIM_SIGNAL_TYPES)

        # Add total signals
        for t, v in cx['signals_type'].items():
            if 'model' in v:
                v['total'] = get_signals_by_user(t, self.request.user).count()

        return cx


class SIIMSignalsListView(TemplateView):
    """ View for show signals list by type """

    template_name = 'signaler_iim/signals_list.html'

    @method_decorator(permission_required('signaler_iim.manage_reports', return_403=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):

        cx = super().get_context_data(**kwargs)

        # Add signals type
        cx['type'] = kwargs['type']
        cx['title'] = SIIM_SIGNAL_TYPES[kwargs['type']]['desc']

        # Add stati color
        cx['stati_color'] = {}
        for s in Stati.objects.all():
            cx['stati_color'][s.desc] = s.color
        cx['stati_color'] = json.dumps(cx['stati_color'])

        try:
            prj = SIIMSignalerProjects.get_project(kwargs['type'], lang=get_language())
            if prj:
                map_url = get_map_url(prj)
                cx['new_signal_url'] = f"{map_url}?sid=new"
            else:
                pass
        except:
            pass

        return cx


class SIIMSignalsDownloadImgView(WeasyTemplateView):
    """ View to download signal data pdf sheet"""

    template_name = "signaler_iim/signal_pdf_sheet.html"
    pdf_stylesheets = [
        os.getcwd() + '/signaler_iim/static/signaler_iim/css/pdf/signal_sheet.css',
    ]
    pdf_attachment = False
    pdf_filename = 'signal_sheet.png'
    content_type = CONTENT_TYPE_PNG

    @method_decorator(permission_required('signaler_iim.manage_reports', return_403=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        cx = super().get_context_data(**kwargs)

        # Retrieve signal instance
        try:
            cx['signal'] = SIIM_SIGNAL_TYPES[kwargs['type']]['model'].objects.get(pk=kwargs['pk'])
        except:
            raise Http404()

        # Build latitude and longitude
        cx['features'] = []

        features = SIIM_SIGNAL_TYPES[kwargs['type']]['geo_model'].objects.filter(signal_id=kwargs['pk'])

        for f in features:

            vxs = SIIM_SIGNAL_TYPES[kwargs['type']]['vx_model'].objects.filter(feature_id=f.pk)

            # For Polygon and LineString geometry type
            # TODO: make point signals case
            ll = []
            for vx in vxs:
                vx.geom.transform(4326)

                # Structure: ('<nome>', '<x>', '<y>'), i.e.: ('A', '44° 03'.354', '08° 04'.456')
                ll.append((vx.nome, dec2dd(vx.geom.x, 'lon'), dec2dd(vx.geom.y, 'lat')))

            cx['features'].append({
                'n': len(vxs),
                'name': f.nome,
                'll': ll,

            })

        return cx


class SIIMSignalsDownloadZipView(View):
    """ A view to download a zip file with shp json csv and txt data format of a Signal """

    @method_decorator(permission_required('signaler_iim.manage_reports', return_403=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def response_7z_file(self, tmp_dir, filename, filenames):
        """ Give filenames build into tmp directory response with 7z file """

        sz_filename = f"{filename}.7z"

        # Open BytesIO to grab in-memory ZIP contents
        s = BytesIO()

        # The 7z compressor
        szf = py7zr.SevenZipFile(s, "w")

        for fpath in filenames:

            # Add file, at correct path
            ftoadd = os.path.join(tmp_dir.name, fpath)
            if os.path.exists(ftoadd):
                szf.write(ftoadd, fpath)

        # Must close zip for all contents to be written
        szf.close()
        tmp_dir.cleanup()

        # Grab ZIP file from in-memory, make response with correct MIME-type
        response = HttpResponse(
            s.getvalue(), content_type="application/x-7z-compressed")
        response['Content-Disposition'] = 'attachment; filename=%s' % sz_filename
        response.set_cookie('fileDownload', 'true')
        return response


    def response_zip_file(self, tmp_dir, filename, filenames):
        """ Give filenames build into tmp directory response with a zip file"""

        zip_filename = f"{filename}.zip"

        # Open BytesIO to grab in-memory ZIP contents
        s = BytesIO()

        # The zip compressor
        zf = zipfile.ZipFile(s, "w")

        for fpath in filenames:

            # Add file, at correct path
            ftoadd = os.path.join(tmp_dir.name, fpath)
            if os.path.exists(ftoadd):
                zf.write(ftoadd, fpath)

        # Must close zip for all contents to be written
        zf.close()
        tmp_dir.cleanup()

        # Grab ZIP file from in-memory, make response with correct MIME-type
        response = HttpResponse(
            s.getvalue(), content_type="application/x-zip-compressed")
        response['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
        response.set_cookie('fileDownload', 'true')
        return response

    def _make_json(self, tmp_dir, filename, filenames, r_features):
        """ Create JSON file into tmp_dir """

        json_filename = f"{filename}.json"

        with open(os.path.join(tmp_dir.name, json_filename), "w", encoding='utf-8') as f:
            json.dump(r_features, f, indent=4)
        filenames.append(json_filename)

    def _make_txt(self, tmp_dir, filename, filenames, r_features):
        """ Create TXT file into tmp_dir """

        txt_filename = f"{filename}.txt"

        with open(os.path.join(tmp_dir.name, txt_filename), "w", encoding='utf-8') as f:
            for rf in r_features:
                for k, v in rf.items():
                    f.write(f"{k}: {v}\n")
                f.write("\n")

        filenames.append(txt_filename)

    def _make_csv(self, tmp_dir, filename, filenames, r_features):
        """ Create CSV file into tmp_dir """

        csv_filename = f"{filename}.csv"

        with open(os.path.join(tmp_dir.name, csv_filename), 'w', encoding='utf-8') as f:
            filewriter = csv.writer(f, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            try:
                filewriter.writerow(r_features[0].keys())
            except:
                pass
            for rf in r_features:
                filewriter.writerow(rf.values())

        filenames.append(csv_filename)

    def _make_shp(self, tmp_dir, filename, filenames, r_features, g_features, vx_features):
        """ Create Shape Files """

        shp_filename = f"{filename}.shp"

        # Create new vectorlayer
        # -------------------------------

        # Create fields
        fields = QgsFields()
        fields.append(QgsField("signal_id", QVariant.Int))
        fields.append(QgsField("nome", QVariant.String))
        fields.append(QgsField("caratteristiche", QVariant.String))
        fields.append(QgsField("vx_nome", QVariant.String))
        fields.append(QgsField("vx_gruppo", QVariant.String))
        fields.append(QgsField("vx_commento", QVariant.String))
        fields.append(QgsField("vx_coord_x", QVariant.String))
        fields.append(QgsField("vx_coord_y", QVariant.String))

        # Save options
        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.driverName = "ESRI Shapefile"
        save_options.fileEncoding = "UTF-8"

        writer = QgsVectorFileWriter.create(
            os.path.join(tmp_dir.name, shp_filename),
            fields,
            QgsWkbTypes.Point,
            QgsCoordinateReferenceSystem("EPSG:4326"),
            QgsCoordinateTransformContext(), #transform context
            save_options
        )

        if writer.hasError() != QgsVectorFileWriter.NoError:
            raise Exception(writer.errorMessage())

        for rf in r_features:

            # add a feature
            fet = QgsFeature()

            vx = vx_features[r_features.index(rf)]
            fet.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(vx.geom.x, vx.geom.y)))
            fet.setAttributes(list(rf.values()))
            writer.addFeature(fet)

        # delete the writer to flush features to disk
        del writer

        for ext in ('.shp', '.shx', '.dbf', '.prj'):
            filenames.append(f"{filename}{ext}")

    def _make_s57_shp(self, tmp_dir, filename, filenames, type, signal, r_features, g_features):
        """ Create Shape Files of type S57 """

        shp_filename = f"{filename}_s57.shp"

        # Create new vectorlayer
        # -------------------------------

        # Create fields
        fields = QgsFields()
        fields.append(QgsField("BUISHP", QVariant.Int))
        fields.append(QgsField("COLOUR", QVariant.Int))
        fields.append(QgsField("COLPAT", QVariant.Int))
        fields.append(QgsField("CONDTN", QVariant.Int))
        fields.append(QgsField("CONRAD", QVariant.Int))
        fields.append(QgsField("ELEVAT", QVariant.Int))
        fields.append(QgsField("FUNCTN", QVariant.Int))
        fields.append(QgsField("HEIGHT", QVariant.Int))
        fields.append(QgsField("NATCON", QVariant.Int))
        fields.append(QgsField("ELEVAT", QVariant.Int))
        fields.append(QgsField("NOBJNM", QVariant.Int))
        fields.append(QgsField("OBJNAM", QVariant.Int))
        fields.append(QgsField("STATUS", QVariant.Int))
        fields.append(QgsField("VERLEN", QVariant.Int))
        fields.append(QgsField("INFORM", QVariant.Int))
        fields.append(QgsField("NINFOM", QVariant.Int))

        # Save options
        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.driverName = "ESRI Shapefile"
        save_options.fileEncoding = "UTF-8"

        writer = QgsVectorFileWriter.create(
            os.path.join(tmp_dir.name, shp_filename),
            fields,
            getattr(QgsWkbTypes, type['geotype']),
            QgsCoordinateReferenceSystem("EPSG:4326"),
            QgsCoordinateTransformContext(), #transform context
            save_options
        )

        if writer.hasError() != QgsVectorFileWriter.NoError:
            raise Exception(writer.errorMessage())

        def get_status():
            """ Return different value by stato value """

            st = signal.stato
            if st in (4,):
                return 12
            elif st in (5, 6):
                return 4
            elif st in (1, 2):
                return 11


        for gf in g_features:

            # add a feature
            fet = QgsFeature()
            fet.setGeometry(QgsGeometry.fromWkt(gf.geom.wkt))
            fet.setAttributes([
                33,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                signal.alt_strutt,
                NULL,
                signal.alt_luce,
                NULL,
                NULL,
                NULL,
                get_status(),
                NULL,
                NULL,
                NULL,
            ])
            writer.addFeature(fet)

        # delete the writer to flush features to disk
        del writer

        for ext in ('.shp', '.shx', '.dbf', '.prj'):
            filenames.append(f"{filename}_s57{ext}")

    def _make_signals_shp(self, tmp_dir, filename, filenames, type, signal):
        """ Create shaper files of feature + fields of parent signal """

        # Get ssim project by type
        siim_prj = SIIMSignalerProjects.get_project(type=type)
        geo_qgs_layer = get_qgis_layer(siim_prj.layer_set.get(origname=SIIM_SIGNAL_TYPES[type]['geo_layer_origname']))
        signal_qgs_layer = get_qgis_layer(siim_prj.layer_set.get(origname=SIIM_SIGNAL_TYPES[type]['signaler_layer_origname']))

        common_fields_to_exclude = [
            'shape',
            'user',
            'passaggio'
        ]

        # Save options
        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.driverName = "ESRI Shapefile"
        save_options.fileEncoding = "UTF-8"
        save_options.attributes = [geo_qgs_layer.fields().indexOf(f.name()) for f in geo_qgs_layer.fields() if f.name() not in common_fields_to_exclude]
        save_options.onlySelectedFeatures = True

        qgs_request = QgsFeatureRequest()
        original_subset_string = geo_qgs_layer.subsetString()

        qgs_request.setFilterExpression(f"{settings.SIIM_SIGNAL_FIELD} = {signal.pk}")

        geo_qgs_layer.selectByExpression(qgs_request.filterExpression().expression())

        file_path = os.path.join(tmp_dir.name, filename)
        error_code, error_message = QgsVectorFileWriter.writeAsVectorFormatV2(
            geo_qgs_layer,
            file_path,
            geo_qgs_layer.transformContext(),
            save_options
        )

        geo_qgs_layer.setSubsetString(original_subset_string)

        if error_code != QgsVectorFileWriter.NoError:
            tmp_dir.cleanup()
            return HttpResponse(status=500, reason=error_message)

        # Add fields with parent signal data
        try:

            # Try to add the layer feature of SearchByPolygon widget
            vlayer = QgsVectorLayer(file_path + '.shp', filename, "ogr")

            if not vlayer:
                raise Exception(f'Failed to load layer {file_path}')

            qgs_request = QgsFeatureRequest()
            original_subset_string = signal_qgs_layer.subsetString()

            qgs_request.setFilterExpression(f"id = {signal.pk}")

            signal_qgs_layer.selectByExpression(qgs_request.filterExpression().expression())

            signal_feature = [f for f in signal_qgs_layer.getSelectedFeatures()][0]

            signal_qgs_layer.setSubsetString(original_subset_string)

            original_attributes_count = len(vlayer.fields())

            assert vlayer.startEditing()

            # Add prefix to field name to avoid repeated names
            # Use 's' as signal
            prefix = 's_'
            for f in signal_feature.fields():
                f.setName(prefix + f.name())
                vlayer.addAttribute(f)

            # Save attributes
            if not vlayer.commitChanges():

                # OGR returns an error if fields were renamed (for example when they are too long)
                # ignore the error in that case
                if 'ERROR: field with index' in str(vlayer.commitErrors()):
                    vlayer.commitChanges()
                else:
                    err_msg = f'Commit error for layer {file_path}'
                    # Check for commit errors
                    errs = vlayer.commitErrors()
                    if len(errs) > 0:
                        errs_msg = ', '.join(errs)
                        err_msg = err_msg + f': {errs_msg}'

                    raise Exception(err_msg)

            # Now add new attribute values
            assert vlayer.startEditing()

            # Add values to new fields
            features = [f for f in vlayer.getFeatures()]
            for feature in features:
                added_fields = {}
                field_index = original_attributes_count
                for f in signal_feature.fields():
                    added_fields.update(
                        {field_index: signal_feature[f.name()]})
                    field_index += 1
                vlayer.changeAttributeValues(feature.id(), added_fields)

            # Commit new fields and exit from editing
            if not vlayer.commitChanges():

                err_msg = f'Commit error for layer {file_path}'
                # Check for commit errors
                errs = vlayer.commitErrors()
                if len(errs) > 0:
                    errs_msg = ', '.join(errs)
                    err_msg = err_msg + f': {errs_msg}'

        except Exception as e:
            logger.error(e)

        for ext in ('.shp', '.shx', '.dbf', '.prj'):
            filenames.append(f"{filename}{ext}")


    def _value_or_blank(self, v):
        """ If v is not None return v """
        return v if v else ''

    def get(self, request, *args, **kwargs):

        try:
            signal = SIIM_SIGNAL_TYPES[kwargs['type']]['model'].objects.get(pk=kwargs['pk'])
        except:
            raise Http404()

        type = SIIM_SIGNAL_TYPES[kwargs['type']]

        # Feature to save into files
        r_features = []
        vx_features = [] # For vertex
        g_features = [] # For SHP and S57 add geo instance

        features = type['geo_model'].objects.filter(signal_id=kwargs['pk'])

        for f in features:

            ll = []
            vxs = type['vx_model'].objects.filter(feature_id=f.pk)
            for vx in vxs:
                vx.geom.transform(4326)
                vx_features.append(vx)

                # Structure: ('<nome>', '<x>', '<y>'), i.e.: ('A', '44° 03'.354', '08° 04'.456')

                r_features.append({
                    'signal_id': f.signal_id,
                    'nome': self._value_or_blank(f.nome),
                    'caratteristiche': self._value_or_blank(f.caratteristiche),
                    'vx_nome': self._value_or_blank(vx.nome),
                    'vx_fruppo': self._value_or_blank(vx.gruppo),
                    'vx_commento': self._value_or_blank(vx.commento),
                    'vx_coord_x': dec2dd(vx.geom.x, 'lon'),
                    'vx_coord_y': dec2dd(vx.geom.y, 'lat')
                })

            f.geom.transform(4326)
            g_features.append(f)


        shp_type = '_S57' if kwargs['mode_export'] != 'zip' else ''
        filename = f"signal_{kwargs['type']}{shp_type}_{kwargs['pk']}"

        # Create a temporary directory for download
        tmp_dir = tempfile.TemporaryDirectory()

        # Filenames to add to zip file
        filenames = []

        if kwargs['mode_export'] == 'zip':
            # Create JSON file
            self._make_json(tmp_dir, filename, filenames, r_features)

            # Create TXT file
            self._make_txt(tmp_dir, filename, filenames, r_features)

            # Create CSV file
            self._make_csv(tmp_dir, filename, filenames, r_features)

            # Create SHP file
            self._make_shp(tmp_dir, filename, filenames, r_features, g_features, vx_features)
        else:

            # Create S57 SHP file
            #self._make_s57_shp(tmp_dir, filename, filenames, type, signal, r_features, g_features)

            self._make_signals_shp(tmp_dir, filename, filenames, kwargs['type'], signal)


        # Return Compressed file
        m = getattr(self, f"response_{settings.SIIM_SIGNAL_DOWNLOAD_COMPRESSION_FORMAT}_file")
        return m(tmp_dir, filename, filenames)





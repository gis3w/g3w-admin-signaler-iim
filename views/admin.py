# coding=utf-8
""""
    Admin views for Signaler IIM moduele.
.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-12-27'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'

from django.views.generic import \
    ListView, \
    CreateView, \
    UpdateView, \
    View
from django.views.generic.detail import SingleObjectMixin
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from guardian.decorators import permission_required
from core.mixins.views import G3WRequestViewMixin, G3WAjaxDeleteViewMixin
from signaler_iim.models import SIIMSignalerProjects
from signaler_iim.forms import SIIMProjectForm


class SIIMProjectsListView(ListView):
    """List of Signaler IIM projects view."""
    template_name = 'signaler_iim/projects_list.html'
    model = SIIMSignalerProjects

    @method_decorator(permission_required('signaler_iim.add_siimsignalerprojects', return_403=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class SSIMProjectViewMixin(object):

    model = SIIMSignalerProjects
    form_class = SIIMProjectForm
    template_name = 'signaler_iim/project_form.html'
    success_url = reverse_lazy('siim-project-list')


class SIIMProjectAddView(SSIMProjectViewMixin, G3WRequestViewMixin, CreateView):
    """
    Create view for single reporting project
    """

    @method_decorator(permission_required('signaler_iim.add_siimsignalerprojects', return_403=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class SIIMProjectUpdateView(SSIMProjectViewMixin, G3WRequestViewMixin, UpdateView):
    """
    Update view for single reporting project
    """

    @method_decorator(
        permission_required('signaler_iim.change_siimsignalerprojects', (SIIMSignalerProjects, 'pk', 'pk'),
                            return_403=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class SIIMProjectDeleteView(G3WAjaxDeleteViewMixin, SingleObjectMixin, View):
    """
    Delete view for single reporting project
    """
    model = SIIMSignalerProjects

    @method_decorator(
        permission_required('signaler_iim.delete_siimsignalerprojects', (SIIMSignalerProjects, 'pk', 'pk'),
                            return_403=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
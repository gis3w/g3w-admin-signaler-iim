# coding=utf-8
"""" Views for user management for Singaler IIM module.

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-02'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.conf import settings
from django.views.generic import \
    CreateView, \
    ListView, \
    UpdateView, \
    View
from django.views.generic.detail import SingleObjectMixin
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import permission_required
from core.mixins.views import G3WRequestViewMixin, G3WAjaxDeleteViewMixin
from usersmanage.models import User
from usersmanage.utils import get_user_groups
from signaler_iim.forms import \
    SIIMUserForm, \
    SIIMUserUpdateForm
import copy


class SIIMUserListView(G3WRequestViewMixin, ListView):
    """List users view."""

    template_name = 'signaler_iim/users/user_list.html'

    @method_decorator(permission_required('signaler_iim.view_siim_users', raise_exception=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):

        # Super user show every Simgaler IIM roles
        if self.request.user.is_superuser:
            groups = settings.SIIM_USERS_GROUP
        else:
            groups = copy.copy(settings.SIIM_USERS_GROUP)
            del(groups['iim_responsible'])

        return User.objects.filter(groups__name__in=groups.values())


class SIIMUserCreateView(G3WRequestViewMixin, CreateView):
    """ Create Signaler IIM User view """

    form_class = SIIMUserForm
    model = User
    template_name = 'signaler_iim/users/user_form.html'

    @method_decorator(permission_required('signaler_iim.create_siim_users', raise_exception=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse('siim-user-list')


class SIIMUserUpdateView(G3WRequestViewMixin, UpdateView):
    """ Update Signaler IIM User view """

    form_class = SIIMUserUpdateForm
    model = User

    # To avoid overwrite 'user' key into general context data.
    context_object_name = 'object'
    template_name = 'signaler_iim/users/user_form.html'

    @method_decorator(permission_required('signaler_iim.update_siim_users', raise_exception=True))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial']['password'] = self.object.password

        # Add initial data for user_groups
        kwargs['initial']['group'] = get_user_groups(self.object)[0]

        # Add initial data for cp phone ecc.. from siimextrauserdata model
        if hasattr(self.object, 'siimuserextradata'):
            kwargs['initial']['cp'] = self.object.siimuserextradata.cp
            kwargs['initial']['iim_office'] = self.object.siimuserextradata.iim_office
            kwargs['initial']['phone'] = self.object.siimuserextradata.phone

        return kwargs

    def get_success_url(self):
        return reverse('siim-user-list')

    def get_context_data(self, **kwargs):
        c = super().get_context_data(**kwargs)

        return c


class SIIMUserAjaxDeleteView(G3WAjaxDeleteViewMixin, G3WRequestViewMixin, SingleObjectMixin, View):
    """ Delete Signaler IIM User ajax view """

    model = User

    @method_decorator(permission_required('signaler_iim.delete_siim_users', raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


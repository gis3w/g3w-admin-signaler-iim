from sitetree.utils import item
from core.utils.tree import G3Wtree





# Be sure you defined `sitetrees` in your module.
sitetrees = (
  # Define a tree with `tree` function.
  G3Wtree('signaler_iim', title='SIGNALER IIM', module='signaler_iim', items=[
      # Then define items and their children with `item` function.
      item('SEGNALAZIONI IIM', '#', type_header=True),
      item('Utenti', '#', icon_css_class='fa fa-users', access_by_perms=["signaler_iim.create_siim_users"], children=[
          item('Aggiungi utente', 'siim-user-add', url_as_pattern=True, icon_css_class='fa fa-plus',
               access_by_perms=['signaler_iim.create_siim_users']),
          item('Lista utenti', 'siim-user-list', url_as_pattern=True, icon_css_class='fa fa-list'),
          item('Agg. utente {{ object.username }}', 'siim-user-update object.pk', url_as_pattern=True,
               icon_css_class='fa fa-edit', in_menu=False, alias='siimsignaleruser-update',
               access_by_perms=['signaler_iim.update_siim_users']),
      ]),
      item('Progetti', '#', icon_css_class='fa fa-users', access_by_perms=["signaler_iim.add_siimsignalerprojects"], children=[
          item('Aggiungi progetto', 'siim-project-add', url_as_pattern=True, icon_css_class='fa fa-plus',
               access_by_perms=['signaler_iim.add_siimsignalerprojects']),
          item('Lista progetti', 'siim-project-list', url_as_pattern=True, icon_css_class='fa fa-list'),
          item('Agg. progetto {{ object.project.title }}', 'siim-project-update object.pk', url_as_pattern=True,
               icon_css_class='fa fa-edit', in_menu=False, alias='siimsignalerprojects-update'),
      ]),
      item('Segnalazioni', 'ssim-signals-dashboard', url_as_pattern=True, icon_css_class='fa fa-map-pin',
           access_by_perms=['signaler_iim.manage_reports']),
  ]),

  G3Wtree('signaler_iim_en', title='SIGNALER IIM', module='signaler_iim', items=[
      # Then define items and their children with `item` function.
      item('SIGNALS IIM', '#', type_header=True),
      item('Users', '#', icon_css_class='fa fa-users', access_by_perms=["signaler_iim.create_siim_users"], children=[
          item('Add user', 'siim-user-add', url_as_pattern=True, icon_css_class='fa fa-plus',
               access_by_perms=['signaler_iim.create_siim_users']),
          item('Users list', 'siim-user-list', url_as_pattern=True, icon_css_class='fa fa-list'),
          item('Update user {{ object.username }}', 'siim-user-update object.pk', url_as_pattern=True,
               icon_css_class='fa fa-edit', in_menu=False, alias='siimsignaleruser-update',
               access_by_perms=['signaler_iim.update_siim_users']),
      ]),
      item('Projects', '#', icon_css_class='fa fa-users', access_by_perms=["signaler_iim.add_siimsignalerprojects"], children=[
        item('Add project', 'siim-project-add', url_as_pattern=True, icon_css_class='fa fa-plus',
               access_by_perms=['signaler_iim.add_siimsignalerprojects']),
          item('Projects list', 'siim-project-list', url_as_pattern=True, icon_css_class='fa fa-list'),
          item('Update project {{ object.project.title }}', 'siim-project-update object.pk', url_as_pattern=True,
               icon_css_class='fa fa-edit', in_menu=False, alias='siimsignalerprojects-update'),
      ]),
      item('Reports', 'ssim-signals-dashboard', url_as_pattern=True, icon_css_class='fa fa-map-pin',
           access_by_perms=['signaler_iim.manage_reports']),
  ]),
)
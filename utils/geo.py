# coding=utf-8
"""" Geo utils for Signaler IIM module.

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-10'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

import math


def dec2dd(dec: float, type: str='lat', mode: str='dm'):
    """
    Give a Floating decimal degrees coordinate return a <degree:int>° <minutes:float>'
    :param dec: Decimal wgs84 coordinate
    :type dec: float
    :param type:
    :param mode: Mode of result 'dm' => <degree:int>° <minutes:float>', 'dms' => <degree:int>° <minutes:int>' <seconds:float>''
    :type mode: str
    :return: Degree minutes representation and/or seconds
    :rtype: str
    """

    compass = {
        'lat': ('N', 'S'),
        'lon': ('E', 'W')
    }

    decimals, number = math.modf(dec)

    d = int(number)
    compass_str = compass[type][0 if d >= 0 else 1]
    if mode == 'dm':
        m = decimals * 60
        return f'{abs(d)}º {abs(m):.3f}\' {compass_str}'
    else:
        m = int(decimals * 60)
        s = (dec - d - m / 60) * 3600.00
        return f'{abs(d)}º {abs(m)}\' {abs(s):.3f}" {compass_str}'



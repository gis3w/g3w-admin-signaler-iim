# coding=utf-8
"""" Help sentences for forms

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-02-10'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.utils.translation import ugettext_lazy as _

help = {
    "sls": {
        "tipo_scheda": " ",
        "presc_marifari": "Citare Protocollo e data lettera",
        "conf_presc": "I segnalamenti installati o modificati senza la prevista approvazione ai sensi della normativa vigente da parte della Direzione dei Fari e dei Segnalamenti non riporteranno nessuna informazione nell'Elenco dei Fari e Segnali da Nebbia I.I.3134 rispettivamente nelle colonne da 4 a 7, ma tutta la caratteristica verrà riportata in colonna 8.",
        "num_ef": "Indicare Numero Nazionale e Internazionale (se presente)",
        "regione": "",
        "provincia": "",
        "comune": "",
        "mare": "",
        "capit_porto": "Specificare Capitaneria di Porto di riferiemnto e competente territorialmente",
        "localita": "",
        "ubicazione": "",
        "tipo_segn": "",
        "altro_tipo_segn": "Inserire la descrizione del segnalamento",
        "caratt_luce": "<p>Gli elementi che consentono l'identificazione dei segnalamenti luminosi sono: il tipo della luce, il colore della luce e il periodo. Per tipo della luce si intende quella caratteristica che deriva dal rapporto, fisso o variabile, fra durata della luce e durata dell'eclisse. Per periodo si intende l'intervallo di tempo entro il quale si sviluppa l'intero ciclo della caratteristica di un segnalamento. </p><p>Vedi I.I.3134 Paragrafo 5 pag. XV.</p>",
        "col_luce": "",
        "periodo": "Espresso in secondi",
        "luce_eclissi": "",
        "struttura": "",
        "col_struttura": "",
        "portata_lum": "Portata nominale espresso in NM",
        "alt_luce": "Altezza della luce sul LMM in metri",
        "alt_strutt": "Altezza della struttura in metri",
        "visibilita": "<p>Indicare l'angolo di visibilit&agrave; del segnalamento, in gradi (a giro di orizzonte o a settori). Per i segnalamenti luminosi con settori di diverso colore e/o con settori oscurati (artificialmente) od occultati (da ostacoli naturali), i settori vengono indicati dai rilevamenti che li definiscono.</p>"
                      "<p>Detti rilevamenti sono veri e presi dal largo e vengono sempre citati in senso orario.</p>"
                      "<p>Esempi:</p>"
                      "<p>VIS 103-300 (197) = visibile, in un settore di 197&deg;, da 103&deg; a 300&deg; (oscurato da 300&deg; a 103&deg;) 103 R 125 W 103 = Fanale bianco, con settore rosso di 22&deg; da 103&deg; a 125&deg; 095 G 110 W 112 R 127 Osc 095 = Fanale a settori: verde da 095&deg; a 110&deg;(15&deg;); rosso da 112&deg; a 127&deg;.</p>",
        "col_settore": "",
        "miraglio": "",
        "tipo_miraglio": "",
        "riflett_radar": "",
        "note_fasi_settore": "Inserire le informazioni relative alla Colonna 8 dell'E.F.",
        "segn_nebbia": "E' un segnalamento acustico emesso per avvertire o guidare le navi in caso di scarsa visibilità.",
        "tipo_segn_nebbia": "",
        "periodo_segn_nebbia": "Espresso in secondi",
        "fase_segn_nebbia": "",
        "segn_morse": "Segnale codificato Morse",
        "portata_segn_nebbia": "Massima distanza della sorgenete sonora in NM",
        "stato": "Stato del segnalamento",
        "soggetto_spegn": "In caso di mancanza di energia elettrica",
        "avurnav": "Presenza AVURNAV",
        "nr_avurnav": "Numero AVURNAV",
        "marifari_comp": "Marifari competente del segnalamento",
        "tipo_concess": "Scelta della tipologia di Concessionario",
        "concess_marifari": "Per i segnalamenti dipendenti dalla Direzione Fari e Segnalamenti Marittimi indicare il Marifari competente",
        "concess_privato": "Inserire dati ed estremi del Concessionario Privato",
        "contatto": "Punto di contatto",
        "titolo_concessorio": "",
        "foto": "<p>Scatto con focale fra 35 e 50 mm a una distanza dal soggetto tale che quest'ultimo riempia quanto pi&ugrave; possibile l'inquadratura.</p>"
                "<p>Risoluzione lato lungo (foto orizz.) almeno 4000 pixel ovvero circa 12 Mpixel. (Inserire Collegamento ipertestuale).</p>",
        "note": "Prescrizioni e note",
        "data": "",
        "link": "",
        "passaggio": "",
        "creazione": "Data autocompilativa",
        "aggiornamento": "Data autocompilativa in caso di aggiornamento scheda",
        "user": ""
    },
    "obs": {
        "tipo_ost": "Tipologia dell'ostacolo",
        "desc": "Inserire la descrizione dell'ostacolo",
        "mod_ril": "Modalità di rilevamento dell'ostacolo",
        "det_fond": "Metodo determinazione dato fondale e battente",
        "det_pos": "Metodo determinazione della posizione",
        "coord_estr": "Inserire le coordinate (ETRF2000) dei punti estremi dell'ostacolo, formato: 000°00'00.000\" N - 000°00'00.000\" E",
        "est_rag": "Raggio dell'eventuale area, valore in metri",
        "est_lun": "Estensione lunghezza, valore in metri",
        "est_lar": "Estensione larghezza, valore in metri",
        "materiale": "Materiale di composizione dell'ostacolo",
        "altro_mat": "Descrizione altro materiale di composizione dell'ostacolo",
        "planimetr": "Documentazione planimetrica",
        "plan_all": "Allegare una planimetria geo-riferita (Datum di riferimento ETRF2000) in formato digitale (file tipo CAD: .dwg, .dgn, .shp)",
        "seg_lum": "Segnalamenti luminosi/sonori? Obbligo compilazione scheda Segnalamenti",
        "divieti": "Zona di Interdizione",
        "ord_all": "Ordinanza di disciplina",
        "id_ord": "Ordinanza di sicurezza - Provvedimento di disciplina, capitaneria compente, numero e data dell'ordinanza (formato: N°  _________ del GG/MM/AAAA)",
        "avurnav": "Presenza AVURNAV",
        "nr_avurnav": "Nr. AVURNAV",
        "note": "Prescrizioni e note"
    },
    "plt": {
        "id_mise": "Indicare il numero di identificazione della piattaforma",
        "id_rid": "Indicare il nome di identificazione della Piattaforma sulla paratia",
        "nome": "Denominazione della piattaforma",
        "anno_cost": "Indicare anno di costruzione",
        "utilizzo": "Specificare la tipologia di utilizzo della piattaforma",
        "tipo_strut": "Specificare la tipologia di struttura della piattaforma",
        "tipo": "Specificare la tipologia della piattaforma",
        "fiamma": "Presenza di una eventuale fiamma (Flare Stack)",
        "carta_iim": "Specificare la capitaneria di porto di riferimento e competente territorialmente.",
        "minerale": "Idrocarburo estratto",
        "concession": "Società Concessionaria",
        "pdc": "Punto di Contatto: N. telefono, Satellitare, telescrivente, HF,...",
        "titoloconc": "Estremi titolo concessorio",
        "pozzi": "Pozzi collegati. Obbligo compilazione scheda Pozzi",
        "n_pozzi": "Numero di pozzi collegati",
        "centrale": "Specificare centrale di riferimento se esistente",
        "coll_centr": "Collegamento alla Centrale? Obbligo compilazione scheda Condutture",
        "coll_attivo": "Spuntare se il collegamento alla Centrale è attivo",
        "coll_altre": "Collegamento con altre piattaforme. Obbligo compilazione scheda Condutture",
        "altre_pf": "Elencare le piattaforme collegate",
        "dist_costa": "Distanza in NM",
        "in_acque_t": "La struttura è installata entro le 12 miglia dalla linea di base.",
        "dist_lbase": "Distanza in NM",
        "aree_prot": "La struttura è in vicinanza di Aree Marine Protette.  ",
        "dist_amp": "Distanza in NM",
        "h_lmm": "Altezza in metri",
        "seg_ident": "In codice Morse",
        "banda_lav": "Indicare Banda e relativa frequenza",
        "periodo": "In secondi",
        "azimut": "In gradi",
        "portata": "In NM",
        "seg_lum": "Segnalamenti luminosi/sonori? Obbligo compilazione scheda Segnalamenti",
        "foa": "Fanali d'Ostacolo Aereo? Obbligo compilazione scheda Segnalamenti",
        "boeproslum": "Boe di prossimità luminose? Obbligo compilazione scheda Segnalamenti",
        "noteboediu": "Indicare caratteristiche e posizione (ETRF2000) aggiungendo una nuova geometria, assegnando un nome significativo e specificando la dicitura 'Boe di prossimità diurne' nel campo commento della geometria",
        "odas": "Indicare se presente una stazione acquisizione dati",
        "tipo_odas": "Indicare il tipo stazione acquisizione dati",
        "note_odas": "Specificare il tipo di dati acquisito e la frequenza di acquisizione",
        "api": "Disponibilità dati online",
        "api_online": "Indicare il link per l'aquisizione dei dati",
        "odas_off": "La Stazione non è conglobata nella piattaforma? Obbligo di compilazione della scheda Segnalamenti",
        "ord_am": "Indicare se presente una Ordinanza della Capitaneria competente che regola, ad esempio, il trasbordo di personale, il servizio di sicurezza, norme particolari, etc. ",
        "hpad_lmm": "Altezza in metri",
        "hpad_estens": "Estensione in mq",
        "dist_ostac": "Distanza in NM",
        "work_freq": "Frequenze divise per VHF, UHF aeronatico, etc.",
        "max_n_pers": "Indicare la presenza di moduli abitativi.",
        "avg_n_pers": "Indicare la massima capacità ricettiva.",
        "elettricita": "Collegamento Elettrico terraferma? Obbligo compilazione scheda Cavi",
        "inacc3176": "I.I.3176 Disciplinare tecnico per la standardizzazione dei rilievi idrografici",
        "dem_dtm": "Indica la presenza del Modello Digitale di Elevazione e/o del Modello Digitale del Terreno",
        "ancoraggi": "Presenti ancoraggi al fondo? Obbligo compilazione scheda Ostacoli",
        "planimetr": "Allegare una planimetria geo-riferita (Datum di riferimento ETRF2000) in formato digitale (file tipo CAD: .dwg, .dgn, .shp).",
        "tracciato": "Allegare una planimetria geo-riferita (Datum di riferimento ETRF2000) in formato digitale (file tipo CAD: .dwg, .dgn, .shp). Allegare elenco AC con coordinate in Datum ETRF2000 in formato digitale (file tipo: .xlsx, .CSV, .TXT)",
        "tracc_altr": "Allegare una planimetria geo-riferita (Datum di riferimento ETRF2000) in formato digitale (file tipo CAD: .dwg, .dgn, .shp). Allegare elenco AC con coordinate in Datum ETRF2000 in formato digitale (file tipo: .xlsx, .CSV, .TXT)",
        "foto": "Allegare OBBLIGATORIAMENTE almeno n. 3 foto da angolazioni diverse e n. 1 foto del nome di identificazione della Piattaforma sulla paratia. Scatto con focale fra 35 e 50 mm a una distanza dalla piattaforma tale che quest’ultima riempia quanto più possibile l’inquadratura.",
        "note": "Risoluzione lato lungo (foto orizz.) almeno 4000 pixel ovvero circa 12 Mpixel."
    },
    "cab": {
        "nome": _("Cable name"),
        "id_tipo": _("Cable type"),
        "concession": _("Concessionaire company"),
        "pdc": _("POC"),
        "tit_conc": _("concession ID"),
        "connotaz": _("Geographic area"),
        "anno_serv": _("In Service date"),
        "stato": _("Status"),
        "verifica": _("Last check date"),
        "ano_cato": _("Draw the corresponding geometry and indicate the anodes and cathodes in the appropriate fields"),
        "seg_lum": _("If it exists, also fill in the appropriate light form"),
        "planimetr": _("Drawing documentation"),
        "allegati": _("Attach the geo-referenced drawings (in the ETRF2000 reference datum) in digital format (CAD type file: .dwg, .dgn, .shp) and the file with the route position list (in the ETRF2000 reference datum) and the characteristics of the cable (file type: .xlsx, .xls, .csv, .txt, .pdf)"),
        "dati": _("Availability of hydro-oceanographic data"),
        "ii_3176": _("I.I.3176 Technical specification for the standardization of hydrographic surveys"),
        "dati_share": _("Data to be shared in accordance with the DPR 15 March 2010 n. 90, Art. 222"),
        "sbp": _("Documentation of the stratigraphic survey with sub bottom profiler"),
        "tre_d": _("Processed by multibeam in accordance with Annex 2 of the Technical specification for the standardization of hydrographic surveys I.I. 3176"),
        "sss": _("Side scan sonar"),
        "vf": _("Video recording, photos"),
        "zona_reg": _("To be flagged if an ordinance has been issued"),
        "id_ord": _("Number and date of the ordinance (formatted: N ° _________ of DD/MM/YYYY)"),
        "data": _("Date of the update or adding to the database"),
        "note": _("Requirements and notes"),
    },
    "wre": {
        "id_ts": " ",
        "id_regione": "Regione",
        "id_prov": "Provincia",
        "id_comune": "Comune",
        "localita": "Località",
        "tipo_seg": "Tipologia segnalamento",
        "tipo_rel": "Tipologia relitto",
        "data_affon": "Inserire se conosciuta",
        "nome": "Nome imbarcazione",
        "materiale": "Materiale imbarcazione",
        "larghezza": "Larghezza relitto valore in metri",
        "lunghezza": "Lunghezza relitto valore in metri",
        "altezza": "Altezza relitto valore in metri",
        "staz_disl": "Stazza o dislocamento in tonnellate",
        "carico": "Natura del carico",
        "combust": "Tipologia combustibile",
        "serbatoio": "Capacità serbatoio in tonnellate o litri",
        "nazione": "Dati matricolari - Nazionalità",
        "comp_ascr": "Dati matricolari - Compartimento di ascrizione",
        "soc_arm": "Dati matricolari - Società armatrice",
        "matricola": "Dati matricolari - Matricola",
        "id_scop": "Modalità scoperta relitto",
        "rinvenim": "Affidabilità identificazione relitto",
        "coord_trnc": "Posizione tronconi inserire le coordinate (ETRF2000), formato:  000°00'00.000'' N - 000°00'00.000'' E. Aggiungere una nuova geometria, assegnando un nome significativo e specificando la dicitura 'Tronconi' nel campo commento della geometria",
        "pos_relat": "Rilevamento vero (Gradi) e distanza (NM) da punto cospicuo su cartografia IIM",
        "punto_cosp": "Nome punto cospicuo ed indicazione numero ed edizione cartografia utilizzata",
        "det_posiz": "Metodo determinazione della posizione",
        "fondale": "Fondale in metri",
        "battente": "Battente libero in metri",
        "det_fond": "Metodo determinazione dato fondale e battente",
        "stato": "Stato del relitto",
        "disposiz": "Disposizione del relitto",
        "tipo_fond": "Natura del fondo",
        "orientamen": "Orientamento scafo longitudinale prora-poppa",
        "storico": "Spuntare se relitto storico",
        "ord_storic": "Ordinanza di disciplina",
        "n_ord_stor": "Ordinanza di sicurezza - Provvedimento di disciplina, capitaneria compente, nr. ordinanza e data emissione relativa al relitto storico",
        "seg_lum": "<p>Segnalamenti luminosi/sonori? Obbligo compilazione scheda Segnalamenti<\p><p>NB: al fine della compilazione delle Schede DDNN, si rappresenta che la mancata, incompleta o errata trasmissione dei dati, comporterà la non presa a carico da parte dell'IIM della documentazione inviata, con conseguente mancato aggiornamento dei Documenti Nautici.<\p>",
        "ordinanza": "Ordinanza di sicurezza",
        "all_ord": "Allegare ordinanza di disciplina",
        "nr_ord": "Ordinanza di sicurezza - Provvedimento di disciplina, capitaneria compente, nr. ordinanza e data emissione",
        "foto": "<p>Allegare alcune foto significative da diverse angolazioni. Scatto con focale fra 35 e 50 mm a una distanza dal relitto tale che quest’ultima riempia quanto più possibile l’inquadratura.<\p><p>Risoluzione lato lungo (foto orizz.) almeno 4000 pixel ovvero circa 12 Mpixel.<\p>",
        "note": "Prescrizioni e note"
    },
    "aeg": {
        "id_rid": "Indicare l'identificativo dell'aerogeneratore",
        "nome": "Denominazione aerogeneratore",
        "carta_iim": "<p>Indicare la Carta di riferimento edita dall'IIM. <a href=\"\media_user\help\carta_iim.jpeg\" target=\"_blank\">Verifica i codici qui</a></p>",
        "pe": "Denominazione Parco Eolico",
        "coord_pe": "Inserire le coordinate (ETRF2000) dei vertici del Parco Eolico, formato:  000°00'00.000'' N - 000°00'00.000'' E",
        "id_regione": "Regione Amministrativa",
        "id_prov": "Provincia Amministrativa",
        "id_comune": "Comune Amministrativo",
        "capit_porto": "Specificare la Capitaneria di Porto di riferimento e competente territorialmente.",
        "conc": "Società Concessionaria",
        "pdc": "Punto di Contatto: N. telefono, Satellitare, telescrivente, HF,...",
        "titoloconc": "Estremi titolo concessorio",
        "dist_shore": "Distanza dalla costa in NM",
        "entro_at": "La struttura è installata entro  le Acque Territoriali e le 12 miglia dalla linea di base",
        "dist_lb": "Distanza dalla linea di base in NM",
        "aree_mp": "La struttura è in vicinanza di Aree Marine Protette",
        "dist_amp": "Distanza dalle Aree Marine Protette in NM",
        "anno_cost": "Indicare l'anno di costruzione",
        "nazionalit": "Nazionalità dell'installazione",
        "tipo_aer": "<p>Specificare la tipologia dell'aerogeneratore. <a href=\"\media_user\help\tipo_aerogen2.jpeg\" "
                    "target=\"_blank\">Verifica il tipo qui</a></p>",
        "tipo_fond": "Specificare la tipologia della fondazione dell'aerogeneratore",
        "h_mozzo": "Altezza del mozzo in m",
        "h_torre": "Altezza della torre in m",
        "h_tot": "Altezza totale in m (altezza al mozzo più lunghezza pala)",
        "h_torre_bm": "Altezza torre dell'aerogeneratore fisso con BM in m",
        "h_torre_am": "Altezza torre dell'aerogeneratore fisso con AM in m",
        "h_max_esc": "Massima escursione verticale dell'aerogeneratore gallegiante in m",
        "max_lato": "Massima escursione laterale dell'aerogeneratore gallegiante in m",
        "h_lmm": "Altezza media dell'aerogeneratore gallegiante sul LMM in m",
        "colore": "Colore dell'aerogeneratore",
        "marca_mod": "Marca e modello aerogeneratore",
        "potenza_mw": "Potenza generatore in MW",
        "pot_nom": "Potenza nominale in Kw",
        "v_cut_in": "Velocità del vento cut-in in m/s",
        "v_nom": "Velocità del vento nominale in m/s",
        "v_cut_out": "Velocità del vento cut-out in m/s",
        "lun_pale": "Lunghezza pale in m",
        "diam_pale": "Diametro pale in m",
        "area_spaz": "Area spazzata in mq",
        "v_rotaz": "Velocita di rotazione range min/max in giri/min",
        "em_sonora": "Massima emissione sonora in db",
        "a_con_cavi": "Elencare gli aerogeneratori collegati con cavi",
        "tracciato": "Tracciato Collegamento tra aerogeneratori. Allegare una planimetria geo-riferita in formato digitale (file tipo CAD: .dwg, .dgn, .shp)",
        "trasformaz": "Collocazione centrale di trasformazione, obbligo compilazione scheda Cavi",
        "centr_mare": "Tracciato Collegamento con Centrale a mare. Allegare una planimetria geo-riferita in formato digitale (file tipo CAD: .dwg, .dgn, .shp)",
        "centr_terr": "Tracciato Collegamento con Centrale a terra. Allegare una planimetria geo-riferita in formato digitale (file tipo CAD: .dwg, .dgn, .shp)",
        "planim_pe": "Planimetria Parco Eolico. Allegare una planimetria geo-riferita in formato digitale (file tipo CAD: .dwg, .dgn, .shp)",
        "fondale": "Profondità fondale nella posizione dell'aerogeneratore sul LMM in m",
        "batim_fond": "È presente un batimetria del fondale?",
        "inacc3176": "In accordo al Disciplinare tecnico dei rilievi idrografici II 3176",
        "data_share": "I dati sono stati condivisi con l'IIM? Compilare la scheda Documentazione",
        "dem_dtm": "È presente un DEM/DTM dell'area? Indica la presenza del Modello Digitale di Elevazione e/o del Modello Digitale del Terreno",
        "ancoraggi": "Sono presenti ancoraggi al fondo? Se sì, obbligo compilazione scheda Ostacoli",
        "nr_ancorag": "Numero di ancoraggi al fondo",
        "vis_radar": "L'aerogeneratore è visibile al radar?",
        "rifl_radar": "È presente un riflettore radar?",
        "beacn_trsp": "È presente un beacon transponder?",
        "seg_ident": "Segnale d'identificazione in codice Morse",
        "banda_lav": "Banda di lavoro e relativa frequenza",
        "periodo": "Periodo in secondi",
        "azimut": "Azimut in gradi",
        "portata": "Portata in miglia",
        "rds": "È presente su Radioservizi per la Navigazione II 3128?",
        "ais_aton": "È presente un AIS Aton?",
        "sbs": "Dettagli Servizio Base Station",
        "tipo_aton": "Tipologia AtoN",
        "tipo_msg": "Tipologia Messaggio",
        "dsc_vhf": "È presente un apparato DSC VHF?",
        "id_mmsi": "Identificativo MMSI",
        "dsc_mf": "È presente un apparato DSC MF?",
        "seg_lum": "È presente un segnalamento luminoso? Obbligo compilazione scheda Segnalamenti",
        "fari_neb": "È presente su Elenco Fari e Segnali da nebbia II3134?",
        "foa": "Fanali d'Ostacolo Aereo? Obbligo compilazione scheda Segnalamenti",
        "boeproslum": "Boe di prossimità luminose? Obbligo compilazione scheda Segnalamenti",
        "boeprosdiu": "Boe di prossimità diurne?",
        "noteboediu": "E' presente una stazione acquisizione dati idro-oceanografici? ",
        "odas": "E' presente una boa acquisizione dati idro-oceanografici? Obbligo compilazione scheda Segnalamenti",
        "boa_odas": "E' presente una boa acquisizione dati idro-oceanografici? Obbligo compilazione scheda Segnalamenti",
        "staz_odas": "Stazione acquisizione dati conglobata nell'aerogeneratore? ",
        "tipo_odas": "Tipologia acquisizione dati idro-oceanografici",
        "note_odas": "Note acquisizione dati idro-oceanografici",
        "api": "Application Programming interface",
        "api_online": "Disponibilità dati online API (Collegamento ipertestuale)",
        "free_acces": "Accesso libero ai dati online",
        "account": "Accesso controllato ai dati online",
        "ord_am": "Sono presenti Ordinanze dell'Autorità Marittima?",
        "ordinanze": "Ordinanze dell'AM che regolano servizi verso l'aerogeneratore. Indicare se presente una ordinanza della Capitaneria competente che regola, ad esempio, il trasbordo di personale, il servizio di sicurezza, norme particolari, etc...",
        "foto": "Allegare OBBLIGATORIAMENTE almeno n. 3 foto da angolazioni diverse e n. 1 foto del nome di identificazione dell'aerogeneratore sulla torre. Scatto con focale fra 35 e 50 mm a una distanza dell'aerogeneratore tale che quest’ultimo riempia quanto più possibile l’inquadratura. <\br> Risoluzione lato lungo (foto orizz.) almeno 4000 pixel ovvero circa 12 Mpixel.",
        "annotazioni": "Prescrizioni e note"
    },
    "byf": {
        "concession": "Società Concessionaria",
        "pdc": "Punto di Contatto: N. telefono, Satellitare, telescrivente, HF,...",
        "tit_conc": "Estremi titolo concessorio",
        "nr_boe": "Totale numero boe del campo",
        "stagionale": "Campo boe con installazione stagionale",
        "data_inst": "Data installazione campo boe",
        "data_rimoz": "Data rimozione Campo Boe ",
        "note_cb": "Note e prescrizioni campo boe",
        "zona_reg": "Spuntare se  vige un Ordinanza",
        "all_ord": "Allegare ordinanza di disciplina",
        "id_ord": "Ordinanza di sicurezza - provvedimento di disciplina, capitaneria competente, numero e data dell'ordinanza (formato: N°  _________ del GG/MM/AAAA)",
        "note": "Prescrizioni e note"
    },
    "pip": {
        "id_tipo": _("Pipeline type"),
        "connotaz": _("Geographic area"),
        "cod_cond": _("Id of the pipeline"),
        "stato": _("Status"),
        "concession": _("Concessionaire company"),
        "pdc": _("POC"),
        "tit_conc": _("Identifiers of the concession"),
        "diffusori": _("Draw the corresponding feature, giving it a meaningful name and specifying 'Pipeline diffusers' in the comment field"),
        "planimetr": _("Drawing documentation"),
        "plan_all": _("Attach the geo-referenced drawing (in the ETRF2000 reference datum) in digital format (CAD type file: .dwg, .dgn, .shp) and the file with the route position list (in the ETRF2000 reference datum) and the characteristics of the pipeline (file type: .xlsx, .xls, .csv, .txt, .pdf)"),
        "dati": _("Availability of hydro-oceanographic data"),
        "ii_3176": _("I.I.3176 Technical specification for the standardization of hydrographic surveys"),
        "dati_share": _("Data to be shared in accordance with the DPR 15 March 2010 n. 90, Art. 222"),
        "sbp": _("Documentation of the stratigraphic survey with sub bottom profiler"),
        "tre_d": _("Processed by multibeam in accordance with Annex 2 of the Technical specification for the standardization of hydrographic surveys I.I. 3176"),
        "sss": _("Side scan sonar"),
        "vf": _("Video recording, photos"),
        "zona_reg": _("To be flagged if an ordinance has been issued"),
        "id_ord": _("Number and date of the ordinance (formatted: N ° _________ of DD/MM/YYYY)"),
        "seg_lum": _("If it exists, also fill in the appropriate light form"),
        "data": _("Date of the update or adding to the database"),
        "note": _("Requirements and notes")
    },
    "dae": {
        "id_adsp": "Autorità di Sistema Portuale. <\br>AVVERTENZA: Nel caso in cui nel menù a tendina NON fosse elencato nessun AdSP occorre fare riferimento ad altro ente di Competenza di Lavoro Portuale",
        "id_porto": "Porto dell'Autorità di Sistema Portuale",
        "concession": "Dati privato compente dragaggio scavo",
        "profondita": "Profondità area dragaggio/scavo",
        "prof_norma": "Profondità a norma Disciplinare Tecnico I.I.3176",
        "livello": "Livello Datum Verticale di riferimento",
        "prof_pescg": "Pescaggio massimo consentito",
        "rilievo": "Eseguito rilievo idrografico",
        "ii_3176": "I.I.3176 Disciplinare tecnico per la standardizzazione dei rilievi idrografici",
        "id_rilievo": "Identificativo rilievo idrografico e descrizione modalità esecuzione",
        "appaltator": "Soggetto esecutore lavori",
        "info_appal": "Nominativo, ragione sociale, PDC",
        "zona_reg": "Spuntare se  vige un ordinanza",
        "all_ord": "Allegare ordinanza di sicurezza",
        "id_ord": "Ordinanza di sicurezza - Provvedimento di disciplina, capitaneria compente, nr. ordinanza e data emissione  (formato: N°  _________ del GG/MM/AAAA)",
        "note": "Prescrizioni e note"
    },
    "iin": {
        "acque_int": "Spuntare SOLO per le notizie riguardanti le acque esterne al mare territoriale senza inserire la Capitaneria di giurisdizione.",
        "nr_in": "Inserire il numero progressivo del blocco a tre cifre,  formato ex. 004.",
        "titolo_in": "Il titolo è formato dal nome della Località e dall'oggetto dell'Informazione",
        "info_fonte": "Nominativo, Ragione Sociale, PdC, n. protocollo, riferimenti informazione",
        "nr_scheda": "Di competenza IIM per le nuove informazioni"
    },
    "pws": {
        "id_adsp": "Autorità di Sistema Portuale. <\br> AVVERTENZA: Nel caso in cui nel menù a tendina NON fosse elencato nessun AdSP occorre fare riferimento ad altro ente di Competenza di Lavoro Portuale",
        "id_porto": "Porto dell'Autorità di Sistema Portuale",
        "concession": "Dati, estremi società concessionaria competente lavoro portuale",
        "tipo": "Tipologia del porto",
        "plan_all": "<p>Allegare planimetria vettoriale di progetto in uno dei seguenti formati: dwg, dgn, dxf, shp, rappresentando: in <span style=\"background-color:#00FF00\">verde</span> le opere già realizzate indicando la data dello stato dei lavori, in <span style=\"background-color:#FFFF00\">giallo</span> le opere in corso di realizzazione, in <span style=\"background-color:#FF0000\">rosso</span> le opere inserite a progetto ancora da realizzare</p>",
        "lotti": "Se i lavori sono divisi in lotti inserire la descrizione degli interventi, le scadenze e/o i rilievi intermedi",
        "data_fine": "Data presunta o termine lavori",
        "appaltator": "Soggetto esecutore lavori",
        "info_appal": "Nominativo, Ragione Sociale, PDC",
        "seg_lum": "È presente un segnalamento luminoso? Obbligo compilazione scheda Segnalamenti",
        "zona_reg": "Spuntare se  vige un Ordinanza",
        "all_ord": "Allegare ordinanza di sicurezza",
        "id_ord": "Ordinanza di sicurezza - Provvedimento di disciplina, capitaneria compente, nr. ordinanza e data emissione  (formato: N°  _________ del GG/MM/AAAA)",
        "note": "Prescrizioni e note"
    },
    "pla": {
        "plan_all": "<p>Planimetria allegata geo-riferita (Datum di riferimento ETRF2000) in formato digitale (file tipo CAD: .dwg, .dgn, .shp). <b>Se la planimetria è di progetto dovrà indicare le opere nel seguente modo:</b> in <span style=\"background-color:#00FF00\">verde</span> le opere già realizzate indicando la data dello stato dei lavori, in <span style=\"background-color:#FFFF00\">giallo</span> le opere in corso di realizzazione, in <span style=\"background-color:#FF0000\">rosso</span> le opere inserite a progetto ancora da realizzare</p>",
        "data_val": "La planimetria rapprensenta lo stato dei luoghi alla data immessa",
        "id_trasmis": "Identificativo trasmissioni precedenti (formato: Foglio n° _______ del GG/MM/AAAA)",
        "id_var": "Identificativo varianti in corso d'opera (formato: Foglio n° _______ del GG/MM/AAAA)",
        "zona_reg": "Spuntare se  vige un ordinanza",
        "all_ord": "Ordinanza di disciplina allegata",
        "id_ord": "Numero e data dell'ordinanza (formato: N°  _________ del GG/MM/AAAA)",
        "aann": "Avviso ai Naviganti",
        "id_aann": "Identificativo Avviso ai Naviganti (formato: A.N. n°__ del Fascicolo __/AAAA)",
        "id_av": "Identificativo AVURNAV (formato: AVURNAV n° ________)",
        "note": "Note e commenti"
    },
    "wel": {
        "id_piattaf": "Piattaforma collegata al pozzo",
        "identif": "Codice identificativo pozzo",
        "prodotto": "Idrocarburo estratto",
        "stato": "Stato produttività del pozzo",
        "zona": "<p>Indicare la zona di riferimento. <a href=\"\media_user\help\zone_marine.jpeg\" target=\"_blank\">Verifica le zone qui</a></p>",
        "sez_unmig": "<p>Indicare la sezione UNMIG. <a haref=\"\media_user\help\\unmig.jpeg\" target=\"_blank\">Verifica la sezione qui</a></p>",
        "carta_iim": "<p>Indicare la Carta di riferimento edita dall'IIM. <a href=\"\media_user\help\carta_iim.jpeg\" target=\"_blank\">Verifica i codici qui</a></p>",
        "campo": "Campo di produzione idrocarburi",
        "centrale": "Centrale di riferimento",
        "concession": "Società Concessionaria",
        "pdc": "Punto di Contatto",
        "titolomin": "Estremi titolo Minerario",
        "collegam": "Pipeline di collegamento. Obbligo compilazione scheda Condutture",
        "dist_costa": "Distanza in NM",
        "in_acque_t": "Il pozzo è situato entro le 12 miglia dalla linea di base",
        "dist_lbase": "Distanza in NM",
        "aree_prot": "Il pozzo è in vicinanza di Aree Marine Protette",
        "dist_amp": "Distanza in NM",
        "profondita": "Profondità del fondale in metri",
        "altezza": "Altezza della testa di pozzo dal fondale in metri",
        "battente": "Battente libero in metri (misura dalla testa di pozzo alla superficie del LMM)",
        "dim_valv": "Dimensione valvola della testa di pozzo",
        "dim_atvalv": "Dimensione dell'attacco valvola della testa di pozzo (specificare se in mm o pollici)",
        "tipo_a": "Tipologia attacco valvola della testa di pozzo",
        "ii_3176": "I.I.3176 Disciplinare tecnico per la standardizzazione dei rilievi idrografici",
        "dati_share": "Dati condivisi obbligatoriamente a norma del DPR 15 Marzo 2010 n. 90, Art. 222",
        "dem_dtm": "Modello Digitale di Elevazione e/o del Modello Digitale del Terreno",
        "note": "Prescrizioni e note"
    },
    "fsa": {
        "stagionale": "Zona ittica con installazione stagionale",
        "data_inst": "Data installazione zona ittica",
        "data_rimoz": "Data rimozione zona ittica",
        "portata": "In tonnellate, quantità di specie ittica prodotta",
        "concession": "Società concessionaria",
        "pdc": "Punto di contatto",
        "tit_conc": "Estremi titolo minerario",
        "data_fine": "Data scadenza concessione",
        "seg_lum": "È presente un segnalamento luminoso? Obbligo compilazione scheda Segnalamenti",
        "planimetr": "Documentazione planimetrica presente",
        "plan_all": "Allegare una planimetria geo-riferita (Datum di riferimento ETRF2000) in formato digitale (file tipo CAD: .dwg, .dgn, .shp).",
        "zona_reg": "Spuntare se  vige un ordinanza",
        "all_ord": "Allegare ordinanza di sicurezza",
        "id_ord": "Ordinanza di sicurezza - Provvedimento di disciplina, capitaneria compente, nr. ordinanza e data emissione  (formato: N°  _________ del GG/MM/AAAA)",
        "note": "Prescrizioni e note"
    }
}
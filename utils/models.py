# coding=utf-8
"""" Model Utility for Signaler IIM module.

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-01-12'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.conf import settings
from django.db.models import Q
from usersmanage.utils import userHasGroups
from signaler_iim.models import SIIM_SIGNAL_TYPES, SlsbCp


def get_signals_by_user(type: str, user, lang='it'):
    """
    Return a QS of a signal model by request user
    :param type: signal type
    :param user: Instance of User model
    :return: QueryString
    """

    model = SIIM_SIGNAL_TYPES[type]['model']

    if userHasGroups(user, [settings.SIIM_USERS_GROUP['ccp_reporter'], settings.SIIM_USERS_GROUP['ccp_supervisor']]):

        # For user with CP set
        args = Q(user=user.pk)
        if hasattr(user, 'siimuserextradata'):
            args = args | Q(capit_porto=SlsbCp.objects.get(pk=user.siimuserextradata.cp).id)
        return model.objects.filter(args)
    elif userHasGroups(user, [settings.SIIM_USERS_GROUP['iim_responsible'], settings.SIIM_USERS_GROUP['iim_examiner']]):
        return model.objects.filter(~Q(passaggio__in=['1IL', '2DV']))
    else:
        return model.objects.all()


def get_acenstor(segn_pad_id=None):
    """
    Give a segn_pad_id return the ancestor or a father->son series.
    :param segn_pad_id: str a string with union of signal id and signal type i.e. 1:aeg
    :return: tuple fo instance of signal and type
    """

    if segn_pad_id:
        signal_id, signal_type = segn_pad_id.split(':')

    def get_father(pk, t):
        """ Recorsive function to get father by son """

        ff = SIIM_SIGNAL_TYPES[t]['father_field']
        m = SIIM_SIGNAL_TYPES[t]['model'].objects.get(pk=pk)
        spi = getattr(m, ff)
        if not spi:
            return m, t
        else:
            return get_father(*spi.split(':'))

    return get_father(signal_id, signal_type)


def propagate_state(signal, stype):
    """
    Give a father signal instance (without segn_pad_id value) propagate its state to signal children
    :param signal: Model instance of signal
    :param stype: Str string of signal type
    """

    # Get children signal type
    rst = SIIM_SIGNAL_TYPES[stype]['relation_signal_types'].keys()

    for tc in rst:
        child_model = SIIM_SIGNAL_TYPES[tc]['model']
        children_signal = child_model.objects.filter(segn_pad_id=f'{signal.pk}:{stype}')
        for cs in children_signal:
            cs.passaggio = signal.passaggio
            cs.save()
            propagate_state(cs, tc)



# coding=utf-8
"""" Config util functions and other for Signaler IIM module

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-12-30'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'

from signaler_iim.models import SIIM_SIGNAL_TYPES, SIIMSignalerProjects
from qdjango.utils.urls import get_map_url


def get_signal_type(project_id):
    """
    Return Signaler IIM signal type by qdjango project id
    :param project_id: The pk of a qdjango Project model
    :type project_id: int
    :returns: The type of signal
    :type returns: str
    """

    return SIIMSignalerProjects.objects.get(project_id=project_id).type


def get_layer_orignames(siim_type, siim_prj, mode='all'):
    """
    Return a dict {l.origname: l.qgs_layer_id} for signal layers by signal type
    :param ssim_type: Type of signal type
    :type siim_type: str
    :param siim_prj: Qdjango Project instance
    :type siim_prj: Project
    :param mode: use to set origname layers to retreive
    :type mode: str
    :type return: dict
    """

    keys = [
        'signaler_layer_origname',
        'geo_layer_origname',
        'vertex_layer_origname',
        'state_layer_origname'
        ]

    if mode != 'all':
        keys = keys[:-1]

    stype = SIIM_SIGNAL_TYPES[siim_type]
    lnames = dict(filter(lambda v: v[0] in keys, stype.items()))
    return {l.origname: l.qgs_layer_id for l in siim_prj.layer_set.filter(origname__in=lnames.values())}


def get_map_link(siim_type, siim_signal_pk, lang='it'):
    """
    Return a link to map to zoom to signal

    :param ssim_type: Type of signal type
    :type siim_type: str
    :param ssim_signal: Pk of signal
    :type siim_type: int
    """

    try:
        prj = SIIMSignalerProjects.get_project(siim_type, lang=lang)
        if prj:
            map_url = get_map_url(prj)
            return f"{map_url}?sid={siim_signal_pk}"
        else:
            return None

    except:
        return None


def get_enc_layer_id(siim_prj, enc_origname):
    """
    Find ENC layer into project and return qgs_layer_id
    :param siim_prj: Qdjango Project instance
    :type siim_prj: Project
    :param enc_origname: WMS layer origname
    :type enc_origname: str
    :return: ENC qgis layer id
    """

    try:
        return siim_prj.layer_set.get(origname=enc_origname).qgs_layer_id
    except:
        return ""




# coding=utf-8
""""

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2022-04-07'
__copyright__ = 'Copyright 2015 - 2022, Gis3w'

from django.conf import settings
from usersmanage.models import Group as AuthGroup


def get_role(user):
    """ Given a user return main SIIM role """

    return AuthGroup.objects.filter(user=user, name__in=settings.SIIM_USERS_GROUP.values())[0]

create database signaler_iim owner iim_editor;
create extension postgis;
grant ALL privileges on all functions in schema public to iim_editor;
grant ALL privileges on all tables in schema public to iim_editor;


# coding=utf-8
""""Signale IIM module default settings

.. note:: This program is free software; you can redistribute it and/or modify
    it under the terms of the Mozilla Public License 2.0.

"""

__author__ = 'lorenzetti@gis3w.it'
__date__ = '2021-10-14'
__copyright__ = 'Copyright 2015 - 2021, Gis3w'

from django.utils.translation import ugettext_lazy as _

# Database settings key to use for signals data
SIIM_DBNAME = 'signaler_iim'

# Field to use for relation between alphanumeric signals layer and geodata signals layers
SIIM_SIGNAL_FIELD = 'signal_id'

# Viewers module
SIIM_USERS_GROUP = {
    'ccp_reporter': 'SIIM-CCP-REPORTERS',
    'ccp_supervisor': 'SIIM-CCP-SUPERVISORS',
    'iim_examiner': 'SIIM-IIM-EXAMINERS',
    'iim_responsible': 'SIIM-IIM-RESPONSIBLES',
}

SIIM_SIGNAL_STATE_FIELD = 'passaggio'

# State where user by role can do editing on feature
SIIM_SIGNAL_EDITING_STATE = {
    SIIM_USERS_GROUP['ccp_reporter']: ['1IL', '3RINC'],
    SIIM_USERS_GROUP['ccp_supervisor']: ['2DV'],
    SIIM_USERS_GROUP['iim_examiner']: ['2PU', '2PR'],
    SIIM_USERS_GROUP['iim_responsible']: ['3IE', '3RICO'],
}

# For passage state validation
SIIM_SIGNAL_EDITING_CHANGE_STATE = {
    SIIM_USERS_GROUP['ccp_reporter']: ['1IL', '2DV'],
    SIIM_USERS_GROUP['ccp_supervisor']: ['1IL', '2DV', '2PU', '2PR'],
    SIIM_USERS_GROUP['iim_examiner']: ['3IE'],
    SIIM_USERS_GROUP['iim_responsible']: ['3RINC', '3RICO', '4C', '4A'],
}

# Format for compressed files:
# - zip
# - 7z (default)
SIIM_SIGNAL_DOWNLOAD_COMPRESSION_FORMAT = '7z'

SIIM_SIGNAL_DEFAULT_LANGUAGE_PROJECT = 'it'

# For ENC WMS layer service
# --------------------------
SIIM_ENC_LAYER_ORIGNAME = 'ENC'
SIIM_ENC_LAYER_INFOURL = 'https://geoservice.elmansrl.eu/pkgeoserviceinfo/wms'
SIIM_ENC_LAYER_INFOFORMAT = 'text/html'